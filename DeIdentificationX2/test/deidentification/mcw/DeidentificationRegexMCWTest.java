package deidentification.mcw;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.converters.Param;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(JUnitParamsRunner.class)
public class DeidentificationRegexMCWTest extends TestCase {
	
//			String s = "1/1CamelCase03/23PulmonarY01/9/2014Colonoscopy1/27/14 01-23-34-56-78 01234567 8/2012 2/2009";
			
//			String s  = "119 N. Tennyson. 1414 s. Mequon Rd. 12 n 5th Street";
//			String s2 = "[xxxxx x. xxxxx] .  [xxxxx x. xxxxx]  Rd.  [xxxxx x. xxxxx]  Street";
//			String s00 = addressRegex(s);
//	
//			s = "Colonoscopy 2006Alpha whatever alhaBeta";
//			String s0 = mixedCaseAlphaNumericTransition(s);
//			System.out.println(s + " : " + s0);
//			
//			s = "declined childbirth classes. 4/1: FAS3/21:  Breastfeeding. Reviewed";
//			s0 = dateRegex(s, -1);
//			System.out.println(s + " : " + s0);
//			
//			s = "Mammo: 6-3-13.  5/4/2011 bi mammo (-). 6-5-12. Negative. Had several magnified views";
//			String s2 = dateRegex(s, -10);
//			System.out.println(s + " : " + s2);
//			s2 = compositeRegex(s, -10, blackListMap);
//			System.out.println(s + " : " + s2);
//			
//			//s = "2/2009";
//			s = "12/27/12 12-27-12 7-4, 2013 09/7 10/2014 10/12 9/7 9/2014";
//			s = "Program Coordinator901  N. 9 th Street = Courthouse Rm.   307 A [XXXXX] ,  WI 53233-1967(P)   ";
//			s2 = compositeRegex(s, -10, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "July 9th 2013 July 9th, 2013 July 9 2013 July 18, 2013";
//			s2 = compositeRegex(s, -10, blackListMap);
//			System.out.println(s2);
//			s = "July 9th 2013";
//			s2 = compositeRegex(s, -10, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "July 9th, 2013";
//			s2 = compositeRegex(s, -10, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "July 9 2013";
//			s2 = compositeRegex(s, -10, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "July 18, 2013";
//			s2 = compositeRegex(s, -10, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "July 18, 2013";
//			s2 = compositeRegex(s, 10, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "November 18, 2013";
//			s2 = compositeRegex(s, 10, blackListMap);
//			System.out.println(s + " : " + s2);
//			/////////
//			s = "10/2013";
//			s2 = compositeRegex(s, 10, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "December 2011";
//			s2 = compositeRegex(s, 10, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "6/22/13";
//			s2 = compositeRegex(s, -10, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "10/22/13";
//			s2 = compositeRegex(s, -7, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "jay.urbain@gmail.com";
//			s2 = compositeRegex(s, -7, blackListMap);
//			System.out.println(s + " : " + s2);
//			s = "urbain@msoe.edu";
//			s2 = compositeRegex(s, -7, blackListMap);
//			System.out.println(s + " : " + s2);
//			
//			s = "53217";
//			s2 = compositeRegex(s, -7, blackListMap);
//			System.out.println(s + " : " + s2);
//			
//			s = "53217-1967";
//			s2 = compositeRegex(s, -7, blackListMap);
//			System.out.println(s + " : " + s2);
//			
//			s = "53217 1967";
//			s2 = compositeRegex(s, -7, blackListMap);
//			System.out.println(s + " : " + s2);
//			

	@Test
	public void testMixedCaseAlphaNumericTransition() {
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		String sOrig  = "Colonoscopy 2006Alpha whatever alhaBeta";
		String sDeid = "Colonoscopy 2006 Alpha whatever alhaBeta";
		String sResult = deidentificationRegex.mixedCaseAlphaNumericTransition(sOrig);
		assertEquals( sDeid, sResult);
	}

	@Test
	public void testPhoneNumberRegex() {
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		String sOrig  = "Jay Urbain's phone number is 414-745-5102, or 745-5102, or (414) 745-5102";
		String sDeid = "Jay Urbain's phone number is  [xxx_xxx_xxxx] , or  [xxx_xxxx] , or  [xxx_xxx_xxxx] ";
		String sResult = deidentificationRegex.phoneNumberRegex(sOrig);
		sResult = deidentificationRegex.phoneNumberRegex2(sResult);
		assertEquals( sDeid, sResult);
		
		sOrig  = "Intake (ml) 1180 2225 2144 2314 1224 779    Output (ml) 150 1475 1450 1900 1750 725    Net (ml) 1030 750 694 414 -526 54    Last Weight 112.038 kg (247 lb) 114.08 kg (251 lb 8 oz) 113 kg (249 lb 1.9 oz) 113.7 kg (250 lb 10.6 oz) 113.4 kg (250 lb) 113.1 kg";
		sDeid = sOrig;
		sResult = deidentificationRegex.phoneNumberRegex(sOrig);
		assertEquals( sDeid, sResult);
		
	}

	@Test
	public void testEmailRegex() {
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		String sOrig  = "Jay Urbain's email  is jay.urbain@gmail.com or urbain@msoe.edu";
		String sDeid = "Jay Urbain's email  is  [xxx@xxx.xxx]  or  [xxx@xxx.xxx] ";
		String sResult = deidentificationRegex.emailRegex(sOrig);
		assertEquals( sDeid, sResult);
	}

	@Test
	public void testZipRegex() {
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		String sOrig  = "53217";
		String sDeid = " [xxxxx] ";
		String sResult = deidentificationRegex.zipRegex(sOrig);
		assertEquals( sDeid, sResult);
		
		sOrig  = "53217-1967";
		sDeid = " [xxxxx] ";
		sResult = deidentificationRegex.zipRegex(sOrig);
		assertEquals( sDeid, sResult);
	}

	@Test
	public void testAddressRegex() {
		
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		String sOrig  = "119 N. Tennyson. 1414 s. Mequon Rd. 12 n 5th Street";
		String sDeid = " [xxxxx x. xxxxx] .  [xxxxx x. xxxxx]  Rd.  [xxxxx x. xxxxx]  Street";
		String sResult = deidentificationRegex.addressRegex(sOrig);
		assertEquals( sDeid, sResult);
	}

	
	@Test
	public void testDateRegex() {
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		String sOrig  = "declined childbirth classes. 4/1: FAS3/21:  Breastfeeding. Reviewed";
		String sDeid = "declined childbirth classes.  [3_31] : FAS3/21:  Breastfeeding. Reviewed";
		String sResult = deidentificationRegex.dateRegex(sOrig, -1);
		assertEquals( sDeid, sResult);

		sOrig  = "Mammo: 6-3-13.  5/4/2011 bi mammo (-). 6-5-12. Negative. Had several magnified views";
		sDeid  = "Mammo:  [5_24_13] .   [4_24_2011]  bi mammo (-).  [5_26_12] . Negative. Had several magnified views";
		sResult = deidentificationRegex.dateRegex(sOrig, -10);
		assertEquals( sDeid, sResult);
		
		sOrig  = "(EMLA) 2.5-2.5 %";
		sDeid = sOrig;
		sResult = deidentificationRegex.dateRegex(sOrig, -10);
		assertEquals( sDeid, sResult);
		
		sOrig  = "5-325 MG tablet";
		sDeid = sOrig;
		sResult = deidentificationRegex.dateRegex(sOrig, -10);
		assertEquals( sDeid, sResult);
		
		//sOrig  = "overlying the 4/5 interlaminar space";
		//sDeid = sOrig;
		//sResult = deidentificationRegex.dateRegex(sOrig, -10);
		//assertEquals( sDeid, sResult);
		
		sOrig  = "Number as date. BP: 108/76";
		sDeid = sOrig;
		sResult = deidentificationRegex.dateRegex(sOrig, -10);
		assertEquals( sDeid, sResult);
		
		//sOrig  = "Rates pain 9-10/10";
		//sDeid = sOrig;
		//sResult = deidentificationRegex.dateRegex(sOrig, -10);
		//assertEquals( sDeid, sResult);
		
		sOrig  = "BP: 105/62 102/66 106/68 114/68";
		sDeid = sOrig;
		sResult = deidentificationRegex.dateRegex(sOrig, -10);
		assertEquals( sDeid, sResult);
		
		sOrig  = "HFNC 5-6 liter";
		sDeid = sOrig;
		sResult = deidentificationRegex.dateRegex(sOrig, -10);
		assertEquals( sDeid, sResult);
	}

	private Object[] dateTests() {
		return new Object[][]{
				{"August 7 , 2015", -10, " [7_28_2015] ", "Date Deid with variable whitespace in the punctuation"},
				{"Sept. 2015", -20, " [8_26_2015] ", "Date Deid with truncated Month and year"},
				{"13/12", -10, "13/12", "Month/Day Date Deid - Invalid Month | no deid"},
				{"6 / 33", -10, "6 / 33", "Month/Day Date Deid - Invalid Day | no deid"},
				{"x1 / 10", -5, "x1 / 10", "Month/Day Date Deid - Preceded by letter | no deid"},
				{"1/10th", -5, "1/10th", "Month/Day Date Deid - Followed by letter | no deid"},
				{"5/9", -5, " [5_4] ", "Month/Day Date Deid - Single Digit, no space"},
				{" 12 / 10 ", -15, " [11_25] ", "Month/Day Date Deid - Double Digit"},
				{"x,1 / 10", -15, "x ,  [12_26] ", "Month/Day Date Deid - Preceded by punctuation"},
				{"12/10 1999", -5, " [12_5_1999] ", "Month/Day Year Date Deid"},
				{"12-10 1999", -5, " [12_5_1999] ", "Month-Day Year Date Deid"},
				{"12-10/1999", -5, " [12_5_1999] ", "Month-Day/Year Date Deid"},
				{"12 10/1999", -5, " [12_5_1999] ", "Month Day/Year Date Deid"},
				{"1.1 2. 3 2001", -5, "1. 1 2. 3 2001", "Lab with apx Month [space] Year Date Deid | no deid"},
				{"9 10 1950", -5, " [9_5_1950] ", "Month/Day-Year Date Deid - space and space"},
				{"Mar  of 2011", -25, " [2_18_2011] ", "Month [of] Year Date Deid"},
				{"05/08", -5, " [5_3] ", "Month day with appended '0' "},
				{"05/08/16", -5, " [5_3_16] ", "Month day year with appended '0' "},
				{"11/01", -25, " [10_7] ", "Month day with appended '0' on day"},
				{"01/25", -20, " [1_5] ", "Month day with appended '0' on month"},
				{"2/'15", -20, " [1_26_15] ", "Month year with apostrophe"},
				{"are 05/'08 in", -20, "are  [4_25_8]  in", "Month year with apostrophe appended '0'"},
				{"02.21.14", -20, " [2_1_14] ", "Month day year with period separation (2# year)"},
				{"02.21.2014", -20, " [2_1_2014] ", "Month day year with period separation (4# year)"},
				{"02. 21. 14", -20, " [2_1_14] ", "Month day year with period separation (2# year) (after preprocessor)"},
				{"02. 21. 2014", -20, " [2_1_2014] ", "Month day year with period separation (4# year) (after preprocessor)"},
				{"8.0-14", -20, "8. 0-14", "Lab value that looks date-ish (no died)"},
				{"02 .  04. 04", -20, "02 . 04. 04", "Lab value that looks date-ish (no died) #2"},
				{"april 27th", -20, " [4_7_2018] ", "lowercase month and day"},
				{"feb 27th", -20, " [2_7_2018] ", "lowercase month (abbr.) and day"},
				{"nov. 27th", -20, " [11_7_2018] ", "lowercase month (abbr.) and day"},
		};
	}

	@Test
	@Parameters(method = "dateTests")
	public void testDeidDateWithPP(String idDateText,
								   int dateShift,
								   String expectedDeidDateText,
								   String testDescription){
		DeidentificationRegexMCW deidregex = new DeidentificationRegexMCW();
		idDateText = deidregex.preProcessText(idDateText);
		assertEquals(expectedDeidDateText,
				     deidregex.dateRegex(idDateText, dateShift));
	}

	private Object[] phoneNumberTests() {
		return new Object[][]{
				{"555 555 5555", " [xxx_xxx_xxxx] ", "Phone number with no dashes"},
		};
	}

	private Object[] blacklistTests() {
		return new Object[][]{
				{"This is Foley, Missouri", new String[]{"foley, missouri"}, "This is  [XXXXX] "},
				{"This is a Foley Catheter", new String[]{"foley, missouri"}, "This is a Foley Catheter"},
				{"This is a Foley Mo Catheter" ,new String[]{"foley mo"}, "This is a  [XXXXX]  Catheter"},
				{"appointment as long as his creatinine" ,new String[]{"longton", "long island"}, "appointment as long as his creatinine"}
		};
	}

	@Test
	@Parameters(method = "blacklistTests")
	public void testBlackList(String idText,
							  String[] blacklistItems,
							  String deidText){
		HashMap<String, String> blacklistmap = new HashMap<String, String>();
		for (String blacklistItem: blacklistItems) {
			blacklistmap.put(blacklistItem, blacklistItem);
		}
		assertEquals(deidText,
				new DeidentificationRegexMCW().blackList(idText, blacklistmap));
	}

	@Test
	@Parameters(method = "phoneNumberTests")
	public void testDeidPhoneNumber(String idPhoneText,
								String expectedDeidPhoneText,
								String testDescription){
		assertEquals(expectedDeidPhoneText,
				new DeidentificationRegexMCW().phoneNumberRegex(idPhoneText));
	}

	@Test
	public void testCreateConvertedDate() {
		
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();		
		SimpleDateFormat formatDate = new SimpleDateFormat("MM-dd-yyyy");
		
		int month = 1;
		int day = 15;
		int year = 2014;
		Calendar cal = new GregorianCalendar();
		int dayOffset = 0;
		cal.set(Calendar.MONTH, month-1);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.YEAR, year);
		Calendar calResult =  deidentificationRegex.createConvertedDate(month, day, year, dayOffset);
		String calString = formatDate.format(cal.getTime());
		String calResultString = formatDate.format(calResult.getTime());
		assertEquals( calString, calResultString);
		
		// this month forward
		dayOffset = 10;
		cal.set(Calendar.MONTH, month-1);
		cal.set(Calendar.DAY_OF_MONTH, day+10);
		cal.set(Calendar.YEAR, year);
		calResult =  deidentificationRegex.createConvertedDate(month, day, year, dayOffset);
		calString = formatDate.format(cal.getTime());
		calResultString = formatDate.format(calResult.getTime());
		assertEquals( calString, calResultString);
		
		// this month back
		dayOffset = -10;
		cal.set(Calendar.MONTH, month-1);
		cal.set(Calendar.DAY_OF_MONTH, day-10);
		cal.set(Calendar.YEAR, year);
		calResult =  deidentificationRegex.createConvertedDate(month, day, year, dayOffset);
		calString = formatDate.format(cal.getTime());
		calResultString = formatDate.format(calResult.getTime());
		assertEquals( calString, calResultString);
		
		// Next month this year
		month = 1;
		dayOffset = 20;
		cal.set(Calendar.MONTH, month-1+1);
		cal.set(Calendar.DAY_OF_MONTH, day+20-31);
		cal.set(Calendar.YEAR, year);
		calResult =  deidentificationRegex.createConvertedDate(month, day, year, dayOffset);
		calString = formatDate.format(cal.getTime());
		calResultString = formatDate.format(calResult.getTime());
		assertEquals( calString, calResultString);	
		
		// Previous month this year
		month = 2;
		dayOffset = -20;
		cal.set(Calendar.MONTH, month-1-1);
		cal.set(Calendar.DAY_OF_MONTH, 31 + dayOffset + day);
		cal.set(Calendar.YEAR, year);
		calResult =  deidentificationRegex.createConvertedDate(month, day, year, dayOffset);
		calString = formatDate.format(cal.getTime());
		calResultString = formatDate.format(calResult.getTime());
		assertEquals( calString, calResultString);
		
		// Next month and next year
		month = 12;
		dayOffset = 20;
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, day+20-31);
		cal.set(Calendar.YEAR, 2015);
		calResult =  deidentificationRegex.createConvertedDate(month, day, year, dayOffset);
		calString = formatDate.format(cal.getTime());
		calResultString = formatDate.format(calResult.getTime());
		assertEquals( calString, calResultString);
		
		// Previous month and previous year
		month = 1;
		dayOffset = -20;
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.DAY_OF_MONTH, 31 + dayOffset + day);
		cal.set(Calendar.YEAR, 2013);
		calResult =  deidentificationRegex.createConvertedDate(month, day, year, dayOffset);
		calString = formatDate.format(cal.getTime());
		calResultString = formatDate.format(calResult.getTime());
		assertEquals( calString, calResultString);
	}

	@Test
	public void testConvertDateNameToNumber() {
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		int sDeid = 12;
		int sResult = deidentificationRegex.convertDateNameToNumber("Dec");
		assertEquals( sDeid, sResult);
		
		sDeid = 12;
		sResult = deidentificationRegex.convertDateNameToNumber("Dec.");
		assertEquals( sDeid, sResult);
		
		sDeid = 12;
		sResult = deidentificationRegex.convertDateNameToNumber("December");
		assertEquals( sDeid, sResult);
		
		sDeid = 0;
		sResult = deidentificationRegex.convertDateNameToNumber("Blanuary");
		assertEquals( sDeid, sResult);
	}

	@Test
	public void testMrnRegex() {
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		String sOrig  = "119 N. Tennyson. 1414 s. Mequon Rd. 12 n 5th Street";
		String sDeid = sOrig;
		String sResult = deidentificationRegex.mrnRegex(sOrig);
		assertEquals( sDeid, sResult);
		
		sOrig  = "1/27/2014      Re: Bob Shithead   MRN: 00776373   DOB: 3/27/1943 Dear Colleague, On 1/13/2014 I saw Bob Shithead in the 2nt.  Attached you will find the  copy of the progress notes from the visit for your review.  If you have any questions please feel free to contact me.";
		sDeid  = "1/27/2014      Re: Bob Shithead   MRN:  [xxxxxxxx]    DOB: 3/27/1943 Dear Colleague, On 1/13/2014 I saw Bob Shithead in the 2nt.  Attached you will find the  copy of the progress notes from the visit for your review.  If you have any questions please feel free to contact me.";
		sResult = deidentificationRegex.mrnRegex(sOrig);
		assertEquals( sDeid, sResult);
		
		sOrig = "Patient Information    Patient Name Sex DOB Phone    Serres, Bob (07328644) Male 1/25/1945 111-555-1234";
		sDeid = "Patient Information    Patient Name Sex DOB Phone    Serres, Bob ( [xxxxxxxx] ) Male 1/25/1945 111-555-1234";
		sResult = deidentificationRegex.mrnRegex(sOrig);
		assertEquals( sDeid, sResult);
		
		sOrig = "Robin Dogbreath   MRN: 277762   DOB: 9/8/1956";
		sDeid = "Robin Dogbreath   MRN:  [xxxxxxxx]    DOB: 9/8/1956";
		sResult = deidentificationRegex.mrnRegex(sOrig);
		assertEquals( sDeid, sResult);
		
		sOrig = "DOB:  2/27/1968  MRN:  000047775    Prior to the procedure";
		sDeid = "DOB:  2/27/1968  MRN:   [xxxxxxxx]     Prior to the procedure";
		sResult = deidentificationRegex.mrnRegex(sOrig);
		assertEquals( sDeid, sResult);
	}

	@Test
	public void testProcedureAllCaps() {
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		String sOrig  = "Colonoscopy";
		String sDeid = "COLONOSCOPY";
		String sResult = deidentificationRegex.procedureAllCaps(sOrig);
		assertEquals( sDeid, sResult);
	}

	@Test
	public void testCompositeRegex() {
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		String [] whiteList = new String[] {"pap"};
		Map<String, String> blackListMap = new HashMap<String, String>();
		ArrayList<String>PHI = new ArrayList<>(Arrays.asList("Z1446554"));
		String sOrig  = "Mammo: 6-3-13.  5/4/2011 bi mammo (-). 6-5-12. Negative. Had several magnified views";
		String sDeid = "Mammo :  [5_24_13] .  [4_24_2011]  bi mammo ( ).  [5_26_12] . Negative. Had several magnified views";
		String sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null},PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "Program Coordinator 901  N. 9th Street, Courthouse Rm. 307 A Milwaukee,  WI 53233-1967(P)";
		sDeid  = "Program Coordinator  [xxxxx x. xxxxx]  th Street , Courthouse Rm. 307 A Milwaukee , WI  [xxxxx] (P)";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "[6_29_2013]   [6_29_2013]";
		sDeid  = "[6_29_2013]   [6_29_2013]";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		//TODO Decide if this is valid test.  The preprocessor now takes out `_` so "6 29 2013" appears as valid date to deid.
		//assertEquals( sDeid, sResult);

		sOrig  = "July 9th 2013";
		sDeid = " [6_29_2013] ";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "July 9th, 2013";
		sDeid = " [6_29_2013] ";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "July 9 2013";
		sDeid = " [7_19_2013] ";
		sResult = deidentificationRegex.compositeRegex(sOrig, 10, blackListMap,new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "July 18, 2013";
		sDeid = " [7_8_2013] ";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "10/2013";
		sDeid = " [10_5_2013] ";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "December 2011";
		sDeid = " [12_25_2011] ";
		sResult = deidentificationRegex.compositeRegex(sOrig, 10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "6/22/13";
		sDeid = " [6_12_13] ";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "10/22/13";
		sDeid = " [10_12_13] ";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap,new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "jay.urbain@gmail.com";
		sDeid = "[xxx@xxx. xxx]";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "urbain@msoe.edu";
		sDeid = "[xxx@xxx. xxx]";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap,new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "53217";
		sDeid = " [xxxxx] ";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		sOrig  = "53217-1967";
		sDeid = " [xxxxx] ";
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
		
		//sOrig  = "Intake (ml) 1180 2225 2144 2314 1224 779    Output (ml) 150 1475 1450 1900 1750 725    Net (ml) 1030 750 694 414 -526 54    Last Weight 112.038 kg (247 lb) 114.08 kg (251 lb 8 oz) 113 kg (249 lb 1.9 oz) 113.7 kg (250 lb 10.6 oz) 113.4 kg (250 lb) 113.1 kg";
		sOrig  = "Intake (ml) 1180 2225 2144 2314 1224 779";
		sDeid = sOrig;
		sResult = deidentificationRegex.compositeRegex(sOrig, -10, blackListMap, new String[] {"Urbain,Jay F",null,null,null}, PHI);
		assertEquals( sDeid, sResult);
	}

	private Object[] compositeTests() {
		Map<String, String> blacklist = new HashMap<>();
		blacklist.put("foley, mo","foley, mo");
		blacklist.put("new york city, ny","new york city, ny");
		ArrayList<String> PHI = new ArrayList<>(Arrays.asList("Z1446554"));
		String[] patienName = new String[] {"McClane,John D","John","D","McClane"};
		return new Object[][]{
				// balcklist
				{"It's John McClane walking across the glass in New York City, NY.", -10, blacklist,patienName,PHI, "It's [PATIENT] [PATIENT] walking across the glass in [XXXXX] ."},
				{"CO2 21 12/17/2015", -20, blacklist,patienName,PHI, "CO 2 21  [11_27_2015] "},
				{"It's John D McClane walking across the glass in New York City, NY.", -10, blacklist,patienName,PHI, "It's [PATIENT] [PATIENT] walking across the glass in [XXXXX] ."},
				{"It's John, McClane walking across the glass in New York City, NY.", -10, blacklist,patienName,PHI, "It's [PATIENT] [PATIENT] walking across the glass in [XXXXX] ."},
				{"It's John D. McClane walking across the glass in New York City, NY.", -10, blacklist,patienName,PHI, "It's [PATIENT] [PATIENT] walking across the glass in [XXXXX] ."},
				{"It's John, McClane D walking across the glass in New York City, NY.", -10, blacklist,patienName,PHI, "It's [PATIENT] [PATIENT] walking across the glass in [XXXXX] ."},
				{"It's John, McClane D. walking across the glass in New York City, NY.", -10, blacklist,patienName,PHI, "It's [PATIENT] [PATIENT] . walking across the glass in [XXXXX] ."},
				// age > 89
				{"Age :  194 ", -10, blacklist,patienName,PHI, "Age: [xxx] "},
				{"Age :  100 ", -10, blacklist,patienName,PHI, "Age: [xxx] "},
				{"Age :  94 ", -10, blacklist,patienName,PHI, "Age: [xxx] "},
				{"Age :  89", -10, blacklist,patienName,PHI, "Age : 89"},
				{"109 years ", -10, blacklist,patienName,PHI, "[xxx] years "},
				{"99 yo ", -10, blacklist,patienName,PHI, "[xxx] years "},
				{"99 y.o ", -10, blacklist,patienName,PHI, "[xxx] years "},
				{"99 y.o. ", -10, blacklist,patienName,PHI, "[xxx] years ."},
				{"90 year", -10, blacklist,patienName,PHI, "[xxx] years "},
				//patient name
				{"It's John, McClane J. walking across the glass in New York City, NY.", -10, blacklist,new String[] {"McClane,John Jason","John","Jason","McClane"},PHI, "It's [PATIENT] [PATIENT] . walking across the glass in [XXXXX] ."},
				{"It's John, McClane J walking across the glass in New York City, NY.", -10, blacklist,new String[] {"McClane,John Jason","John","Jason","McClane"},PHI, "It's [PATIENT] [PATIENT] walking across the glass in [XXXXX] ."},
				{"It's John, McClane walking across the glass in New York City, NY.", -10, blacklist,new String[] {"McClane,John Jason","John","Jason","McClane"},PHI, "It's [PATIENT] [PATIENT] walking across the glass in [XXXXX] ."},



		};
	}

	@Test
	@Parameters(method = "compositeTests")
	public void testComposites(String idText,
							   Integer dateShift,
							   Map<String, String> blacklistmap,
							   String[] patientName,
							   ArrayList<String> PHI,
							   String deidText
							   ){

		assertEquals(deidText,
				new DeidentificationRegexMCW().compositeRegex(idText,
						dateShift, blacklistmap, patientName, PHI));
	}
	


	@Test
	public void testPHI_age() {
		ArrayList<String> PHI = new ArrayList<>();
		String sOrig  = "Jay is 76 years old";		//age <89
		DeidentificationRegexMCW.filterAge(PHI,"19-01-2015", "20-01-1938" );
		String sDeid = "Jay is 76 years old";
		String sResult = DeidentificationRegexMCW.filterPhi(sOrig, PHI);
		assertEquals( sDeid, sResult);
	}
	
	
	@Test
	public void testFindMiddleInitial() {
		String[] patienName = new String[] {"Jay","Yo","Urbain"};
		String sOrig  = "Jay Urbain Yo";
		String sOrig2  = "Jay Urbain Y";
		String sDeid = " [PATIENT] [PATIENT] ";
		String sResult = DeidentificationRegexMCW.findMiddleInitial(sOrig, patienName);
		String sResult2 = DeidentificationRegexMCW.findMiddleInitial(sOrig2, patienName);
		System.out.println(sResult);
		System.out.println(sResult2);
	}
	
	private Object[] PHITests() {
		return new Object[][]{
			
				//address
				{"Jay lives in central st Albuquerque 87123", "Jay lives in  [xxxxx]   [xxxxx]   [xxxxx] ", "address"},
				{"Jay lives in central  st Albuquerque 87123", "Jay lives in  [xxxxx]   [xxxxx]   [xxxxx] ", "address"},
				//phone numbers
				{"you can reach him at 5053098022", "you can reach him at  [xxxxx] ", "phone number"},
				{"you can reach him at 505 309-8022", "you can reach him at  [xxxxx] ", "phone number with different seperators"},
				{"you can reach him at 505 309 8022", "you can reach him at  [xxxxx] ", "phone number with different seperators"},
				//SSN
				{"Jay SSN 294-56 0977 is doing well", "Jay SSN  [xxxxx]  is doing well", "SSN"},
				{"Jay SSN 294560977 is doing well", "Jay SSN  [xxxxx]  is doing well", "SSN"},
				//age
				{"Jay is 96 years old", "Jay is  [xxxxx]  years old", "de-identify age > 89"},
				//zip code
				{"Jay zip code 87123", "Jay zip code  [xxxxx] ", "zip code range"},
				
				

				
		};
	}

	@Test
	@Parameters(method = "PHITests")
	public void testPHI(String idDateText,
			   String expectedDeidDateText,
			   String testDescription){
		DeidentificationRegexMCW deidregex = new DeidentificationRegexMCW();
		idDateText = deidregex.preProcessText(idDateText);
		ArrayList<String> PHI = new ArrayList<>(Arrays.asList("central st.", "Albuquerque", "87123-12344", "505-309-8022" ,null,null,"20-01-1918","294-56-0977",null,"19-01-2015"));
		assertEquals(expectedDeidDateText,
				     deidregex.filterPhi(idDateText, PHI));
	}
	
	
	private Object[] PHITestsNotProvided() {
		return new Object[][]{
			
				//address
				{"Jay came to the hospital ..", "Jay came to the hospital . .", "null cases"},
				
		};
	}
	
	@Test
	@Parameters(method = "PHITestsNotProvided")
	public void PHITests2(String idDateText,
			   String expectedDeidDateText,
			   String testDescription){
		DeidentificationRegexMCW deidregex = new DeidentificationRegexMCW();
		idDateText = deidregex.preProcessText(idDateText);
		ArrayList<String> PHI = new ArrayList<>();
		assertEquals(expectedDeidDateText,
				     deidregex.filterPhi(idDateText, PHI));
	}
	
	
	private Object[] PHITests_age() {
		return new Object[][]{
			
				//address
				{"Age:  90 years   Admit date:", "Age :  [xxxxx]  years Admit date :" , "age greater than 89"},
				

				
		};
	}

	@Test
	@Parameters(method = "PHITests_age")
	public void testPHI_age(String idDateText,
			   String expectedDeidDateText,
			   String testDescription){
		DeidentificationRegexMCW deidregex = new DeidentificationRegexMCW();
		idDateText = deidregex.preProcessText(idDateText);
		ArrayList<String> PHI = new ArrayList<>(Arrays.asList(null, null, null,null ,null,null,"20-06-1924",null,null,"26-06-2014"));
		assertEquals(expectedDeidDateText,
				     deidregex.filterPhi(idDateText, PHI));
	}
	
	
	
	
}
