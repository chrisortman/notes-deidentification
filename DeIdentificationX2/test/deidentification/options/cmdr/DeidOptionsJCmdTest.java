/**
 * @author Matthew Hoag
 */
package deidentification.options.cmdr;

import com.beust.jcommander.ParameterException;
import deidentification.options.DeidOptions;
import deidentification.options.DeidOptionsParser;
import deidentification.options.cmdr.cnvrtr.ClassConverter;
import junit.framework.TestCase;

import java.io.File;

public class DeidOptionsJCmdTest extends TestCase{

	public void testHelpCommandLine(){
		DeidOptionsParser parser = new DeidOptionsJCmd();
		String[] argv = {"--help"};

		try {
			parser.parseOptions(argv);
			fail("--help did not raise InvalidOptions");
		}catch (DeidOptionsParser.InvalidOptions invOps)
		{
			assertTrue(invOps.getHelpText() != null);
			assertEquals(0, invOps.getExitCode());
		}
	}

	public void testInvalidCommandLineArg(){
		DeidOptionsParser parser = new DeidOptionsJCmd();
		String[] argv = {"-badarg"};

		try {
			parser.parseOptions(argv);
			fail("-badarg did not raise InvalidOptions");
		}catch (DeidOptionsParser.InvalidOptions invOps)
		{
			assertEquals(ParameterException.class, invOps.getCause().getClass());
			assertTrue(invOps.getHelpText() != null);
			assertTrue(invOps.getExitCode()> 0);
		}
	}

	public void testFileOption(){
		DeidOptionsParser parser = new DeidOptionsJCmd();
		String[] argv = {"-login", "a_user", "-password", "a_password", "-whitelistfilename", "samples/whitelist.txt"};

		try {
			DeidOptions opts = parser.parseOptions(argv);
			File f = opts.getWhitelistfile();
			assertTrue("File provided as an option should exist", f.exists());
			assertTrue("File provided as an option should not be a directory", !f.isDirectory());
		}catch (DeidOptionsParser.InvalidOptions invOps)
		{
			invOps.getCause().printStackTrace();
			fail("All options should be valid");
		}
	}

	public void testClassOption(){
		DeidOptionsParser parser = new DeidOptionsJCmd();
		String[] argv = {"-regexdeidentificationclass", "deidentification.mcw.DeidentificationRegexMCW", "-login", "a_user", "-password", "a_password"};

		try {
			DeidOptions opts = parser.parseOptions(argv);
			assertEquals(deidentification.mcw.DeidentificationRegexMCW.class, opts.getRegexdeidentificationclass());

		}catch (DeidOptionsParser.InvalidOptions invOps)
		{
			invOps.getCause().printStackTrace();
			fail("All options should be valid");
		}
	}

	public void testInvalidClassOption(){
		DeidOptionsParser parser = new DeidOptionsJCmd();
		String[] argv = {"-regexdeidentificationclass", "bad.deidentification.mcw.DeidentificationRegexMCW", "-login", "a_user", "-password", "a_password"};

		try {
			parser.parseOptions(argv);
			fail("Invalid class did not raise InvalidOptions");
		}catch (DeidOptionsParser.InvalidOptions invOps)
		{
			assertEquals(ClassConverter.ClassConverterException.class, invOps.getCause().getClass());
			assertTrue(invOps.getHelpText() != null);
			assertTrue(invOps.getExitCode()> 0);
		}
	}
}
