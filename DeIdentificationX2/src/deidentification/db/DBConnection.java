/**
 *
 */
package deidentification.db;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.microsoft.sqlserver.jdbc.SQLServerPreparedStatement42;
import com.mysql.jdbc.JDBC4PreparedStatement;
import com.opencsv.CSVWriter;

import deidentification.MedicalRecordWrapper;
import deidentification.options.DeidOptions;
import org.jetbrains.annotations.NotNull;

/**
 * @author jayurbain
 * @author Matt Hoag
 *
 */
public class DBConnection {

    Connection connection = null;
    private DBLocalization local_dialect = null;
    private String tableName = null;
    private String dbUrl = null;
    private boolean fileData = false;
    private boolean testMode = false;

    public DBConnection(DeidOptions opts, Properties dbprops) throws Exception {
        
        this.dbUrl = opts.getDburl();
        
        // establish connection
        try {     	
        	if( opts.getDBDriver().equals("org.relique.jdbc.csv.CsvDriver") ) {
        		fileData = true;
                // Define column names and column data types here.
//        		  dbprops.put("suppressHeaders", "true");
//                dbprops.put("headerline", "ID,ANGLE,MEASUREDATE");
//                dbprops.put("columnTypes", "Int,Double,Date");
                dbprops.put("separator", "\t");
        	}
        	this.connection = DriverManager.getConnection(dbUrl, dbprops);
        } catch (Exception e) {
            //TODO use Logging
            System.out.println("Could not connect to the database dburl: "
                    + dbUrl + ", login: " + dbprops.getProperty("login") + ", password: ********");
            throw new Exception(e);
        }
        this.local_dialect = getLocalization();
        this.testMode = opts.testMode();
    }

    public DBLocalization getLocalization() throws SQLException {
        return DBLocalization.Registry.getLocalization(connection);
    }

    public ResultSet runQuery(String query) throws SQLException {
        java.sql.Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        //closeOnCompletion doesn't seem to be implemented on all drivers...
        //Statement lifecycle management should be all that is needed anyways.
        //statement.closeOnCompletion();
        return resultSet;
    }

    public void createDeidNotesTable(String tableName) throws SQLException, InstantiationError {

        if (testMode) {
            System.out.println("***TEST MODE: Creating notes table " + tableName + " for " + getLocalization().getLocalizationID() + " database");
        } else {
            this.tableName = tableName;
            if (!fileData) {
                local_dialect.createDeidNotesTable(connection, tableName);
            }
        }
    }

    /**
     * Update the tableName Specified with the de-identified value
     *
     * @param rList
     * @param preparedInsertQuery
     * @throws SQLException
     */
    public void updateDeidRecord(  String preparedInsertQuery , List<MedicalRecordWrapper> rList) throws SQLException {

        PreparedStatement insertPreparedStatement = null;
        try {
            insertPreparedStatement = connection.prepareStatement(preparedInsertQuery);
            connection.setAutoCommit(false);
            String sqlString = "";
            for (MedicalRecordWrapper r : rList) {
                insertPreparedStatement.setString(1, r.getDeIdText());
                insertPreparedStatement.setString(2, r.getId());
                insertPreparedStatement.addBatch();
                sqlString += preparedInsertQuery.replaceFirst("\\?", r.getDeIdText()).replaceFirst("\\?", r.getId()) + "\n";

            }
            if (testMode) {

                System.out.println("*** TEST MODE " + sqlString);
            } else {
                insertPreparedStatement.executeBatch();
            }
        } finally {
            if (!testMode) {
                connection.commit();
            }
            connection.setAutoCommit(true);
            insertPreparedStatement.close();
        }
    }

    public void insertDeidRecord(String tableName, List<MedicalRecordWrapper> rList) throws SQLException {

        String preparedInsertQuery = "insert into " + tableName
                + " (id, note_id, orig_note_text, regex_note_text, deid_note_text, msecs_ner, msecs_regex)"
                + " values (?, ?, ?, ?, ?, ?, ?)";
        
        if( fileData ) {
        	
        	String filePath = dbUrl.replace("jdbc:relique:csv:", "") + "/" + tableName + ".txt";
            try {
				CSVWriter writer = new CSVWriter(new FileWriter(filePath, true), '\t');
				List<String> record = new ArrayList<String>();
				for (MedicalRecordWrapper r : rList) {
					record.add(r.getId());
					record.add(r.getNoteId());
					record.add(r.getText());
					record.add(r.getRegexText());
					record.add(r.getDeIdText());
					record.add(""+r.getMillisecondsNER());
					record.add(""+r.getMillisecondsRegex());
					String[] array = record.toArray(new String[0]);
					writer.writeNext(array);	    	    
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
        else {
	        PreparedStatement insertPreparedStatement = null;
	        try {
	            insertPreparedStatement = connection.prepareStatement(preparedInsertQuery);
	            connection.setAutoCommit(false);
	            for (MedicalRecordWrapper r : rList) {
	                insertPreparedStatement.setString(1, r.getId());
	                insertPreparedStatement.setString(2, r.getNoteId());
	                insertPreparedStatement.setString(3, r.getText());
	                insertPreparedStatement.setString(4, r.getRegexText());
	                insertPreparedStatement.setString(5, r.getDeIdText());
	                insertPreparedStatement.setLong(6, r.getMillisecondsNER());
	                insertPreparedStatement.setLong(7, r.getMillisecondsRegex());
	                insertPreparedStatement.addBatch();
	            }

	            if (testMode) {

                    String sqlString = insertPreparedStatement.toString();
                    System.out.println("***TEST MODE: " + sqlString);
                } else {
                    insertPreparedStatement.executeBatch();
                }
	        } finally {
	            if (!testMode) {
                    connection.commit();
                }
	        	connection.setAutoCommit(true);
	            insertPreparedStatement.close();
	        }
        }
    }

    public void close() throws SQLException {
        connection.close();
    }
}
