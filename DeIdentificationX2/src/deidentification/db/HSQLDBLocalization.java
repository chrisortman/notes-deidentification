package deidentification.db;

import org.hsqldb.persist.HsqlDatabaseProperties;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Matt Hoag
 */
public class HSQLDBLocalization implements DBLocalization {

    public final static String PRODUCT_NAME = HsqlDatabaseProperties.PRODUCT_NAME;

    @Override
    public void createDeidNotesTable(Connection connection, String tableName) throws SQLException {

        Statement statement = connection.createStatement();

        String query1 = "DROP TABLE " + tableName;
        try {
            statement.executeUpdate(query1);
        } catch (SQLException e) {
            //TODO log that the table doesn't exist (INFO)
        }

        String query2
                = "CREATE TABLE " + tableName + " ( "
                + "id INTEGER NOT NULL "
                + ", note_id VARCHAR(254) DEFAULT NULL "
                + ", orig_note_text VARCHAR(4000) DEFAULT NULL "
                + ", regex_note_text VARCHAR(4000) DEFAULT NULL "
                + ", deid_note_text VARCHAR(4000) DEFAULT NULL "
                + ", msecs_ner INTEGER DEFAULT 0 "
                + ", msecs_regex INTEGER DEFAULT 0 "
                + ", PRIMARY KEY (id) "
                + ")";
        statement.executeUpdate(query2);
    }

    @Override
    public String getLocalizationID() {
        return PRODUCT_NAME;
    }
}
