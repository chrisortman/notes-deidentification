package deidentification.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Matt Hoag
 */
public interface DBLocalization {

  /**
   * The DB Localization static registry
   */
  public class Registry {

    private final static ConcurrentHashMap<String, DBLocalization> registeredLocalization
            = new ConcurrentHashMap<>();

    private Registry() {}

    static {
      loadInitialRegistry();
    }

    private static void loadInitialRegistry() {
      addToRegistry(new MySQLDBLocalization());
      addToRegistry(new HSQLDBLocalization());
      addToRegistry(new PostgresDBLocalization());
      addToRegistry(new OracleDBLocalization());
      addToRegistry(new MsSqlDBLocalization());
    }


    public static void addToRegistry(DBLocalization localization) {
      registeredLocalization.put(localization.getLocalizationID(), localization);
    }

    /**
     * Gets the DBLocalization associated with product name {@link java.sql.DatabaseMetaData#getDatabaseProductName()}
     * of the connection's metadata {@link java.sql.Connection#getMetaData()}
     *
     * @param connection the database connection
     * @return database localization
     * @throws SQLException if metadata is unavailable
     */
    public static DBLocalization getLocalization(Connection connection) throws SQLException {


      final String databaseProductName = connection.getMetaData().getDatabaseProductName();
      if (registeredLocalization.containsKey(databaseProductName)) {
        return registeredLocalization.get(databaseProductName);
      } else {
        throw new RuntimeException("Could not find database localization for '" + databaseProductName + "'");
      }
    }
  }

  /**
   * Creates a table given a connect and a table name #SIDE-EFFECT# Drops the
   * table if it exists
   *
   * @param connection the database connection
   * @param tableName the table to be created
   * @throws SQLException
   */
  public void createDeidNotesTable(Connection connection, String tableName) throws SQLException;

  /**
   * The localization ID associating the Localization (dialect) with the type of Database.
   *
   * @return localization ID
   */
  public String getLocalizationID();
}