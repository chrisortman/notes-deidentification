/**
 * @author Jay Urbain
 *
 * MCW Deidentification - Regular expressions for email, web url's, addresses,
 * dates, phone numbers, dates, MRN - Stanford NLP named entity recognition
 * models for person identification using CRF (Conditional Random Field)
 * 2/14/2014
 */
package deidentification;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

import deidentification.db.DBConnection;

import deidentification.options.DeidOptions;
import deidentification.options.cmdr.DeidOptionsJCmd;
import deidentification.options.DeidOptionsParser;
import util.MsgLog;
import util.SimpleIRUtilities;

/**
 * @author jayurbain
 * @author Matt Hoag
 */
public class DeIdentification {

    public static double EntityRecTotalTime;	
    public static double RegexTotalTime;
    private final static int NumColWithoutPHI = 8;
    
    /**
     * @param args
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) throws IOException {
    	
        final DeidOptionsParser parser = new DeidOptionsJCmd();
        DeidOptions opts = null;

        try {
            opts = parser.parseOptions(args);
        } catch (DeidOptionsParser.InvalidOptions e) {
            System.out.println(e.getHelpText());
            if (e.getCause() != null) {
                System.out.println(e.getCause().getMessage());
                e.getCause().printStackTrace();
            }
            System.exit(e.getExitCode());
        }
        System.out.println("Updateable = " + opts.isUpdateOnly());
        String[] whiteListArray = null;
        String[] blackListArray = null;

        //////////////////////////////////////////////////
        // read white list - pass through list
        try {
            whiteListArray = loadFileList(opts.getWhitelistfile());
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        // read black list - block list
        try {
            blackListArray = loadFileList(opts.getBlacklistfile());
        } catch (IOException e2) {
            e2.printStackTrace();
        }

        Map<String, String> whiteListMap = new HashMap<String, String>();
        Map<String, String> blackListMap = new HashMap<String, String>();

        if (whiteListArray != null && whiteListArray.length > 0) {
            for (int i = 0; i < whiteListArray.length; i++) {
                String[] stringArray = whiteListArray[i].split("\\s+");
                for (int j = 0; j < stringArray.length; j++) {
                    String s = stringArray[j].toLowerCase();
                    whiteListMap.put(s, s);
                }
            }
            System.out.println("WhiteList size: "+whiteListArray.length);
        }
        if (blackListArray != null && blackListArray.length > 0) {
            for (int i = 0; i < blackListArray.length; i++) {
                String[] stringArray = blackListArray[i].split("[\\r\\n]+");
                for (int j = 0; j < stringArray.length; j++) {
                    String s = stringArray[j].toLowerCase();
                    blackListMap.put(s, s);
                }
            }
            System.out.println("BlackList size: "+blackListArray.length);
        }
        
        NamedEntityRecognition namedEntityRecognition = null;
        DeidentificationRegex deidentificationRegex = null;
        MsgLog logFile = new MsgLog(opts.getLogFile());
        try {
            namedEntityRecognition = (NamedEntityRecognition) opts.getNamedentityrecognitionclass().newInstance();
            System.out.println("CLASS TO DEID WITH : " + opts.getRegexdeidentificationclass());
            deidentificationRegex = (DeidentificationRegex) opts.getRegexdeidentificationclass().newInstance();
        } catch (InstantiationException | IllegalAccessException e1) {
            System.out.println("Exception Caught ");
            e1.printStackTrace();
            System.exit(-1);
        }
        namedEntityRecognition.setWhiteListMap(whiteListMap);
        namedEntityRecognition.setBlackListMap(blackListMap);
        ///////////////////////////////////////////////////////////
        java.util.Date startDate = new java.util.Date();
        java.util.Date dbFinishDate = new java.util.Date();
        RegexTotalTime = 0;
        EntityRecTotalTime  = 0;
        int totalRows = 0;
        // load driver, get connection
        final Properties dbProps = new Properties();

        dbProps.put("user", opts.getLogin());
        dbProps.put("password", opts.getPassword());

        DBConnection connection;

        try {
            connection = new DBConnection(opts, dbProps);
        } catch (Exception e) {
            System.out.println("1: Could not connect to the database dburl: " + opts.getDburl()
                    + ", login: " + opts.getLogin() + ", password: " + opts.getPassword());
            e.printStackTrace();
            return;
        }

        // create output deid notes table
        if (!opts.isUpdateOnly()) {

            try {
                connection.createDeidNotesTable(opts.getDeidnotestablename());
            } catch (SQLException e1) {
                e1.printStackTrace();
                return;
            }
        }

        ////////////////////////////////////////////////////////
        // read database records with supplied query
        List<DeIdentificationThread> threadList = new ArrayList<DeIdentificationThread>();

        String selectSql = "NOT INITIALIZED";
        String updateSql = "NOT INITIALIZED";

        try {

            if (opts.getQueryFile() != null) {
                selectSql = readFile(opts.getQueryFile(), Charset.defaultCharset());
            } else {
                selectSql = opts.getQuery();
            }

            if (opts.getUpdateFile() != null) {
                updateSql = readFile(opts.getUpdateFile(), Charset.defaultCharset());
            } else {
                updateSql = opts.getUpdateQuery();
            }

            ResultSet results = connection.runQuery(selectSql);

            DeIdentificationThread newT = new DeIdentificationThread(namedEntityRecognition);
            ResultSetMetaData rsMetaData = results.getMetaData();
            int columnCount = rsMetaData.getColumnCount();
            boolean patNameProvided = false;
            boolean PHIProvided = false;
            if (columnCount >= 8) { // assume pat_name, first, last, middle initial are provided
                patNameProvided = true;
            }
            if (columnCount > NumColWithoutPHI ) { // assume patient PHI are provided
                PHIProvided = true;
                System.out.println("PHI are seeded");
            }

            System.out.println("***");
            dbFinishDate = new java.util.Date();

            int rowCount = 0;

            while (results.next()) {

                rowCount++;

                if (opts.testMode() && rowCount > 5) {
                    continue;
                }

                String id = results.getString(1);
                String note_id = results.getString(2);
                //Supply an empty string instead of null if the Oracle value is "NULL"
                String text = results.getString(3) != null ? results.getString(3) : "";
                int dateOffset = results.getInt(4);
                String[] patientName = new String[4];
                ArrayList<String> patientPHI = new ArrayList<String>();
                
                if (patNameProvided) {
                	for (int i=5; i<=NumColWithoutPHI; i++) {
                		patientName[i-5] = results.getString(i);}
                    
                }
                
                //Look for more PHI
                if (PHIProvided) {
                	patientPHI = findPhi(results);
                }


                if (opts.testMode()) {
                    System.out.println("***TEST MODE: Before PreProcessing");
                    System.out.println(text);
                }

                java.util.Date startDateRegex = new java.util.Date();
                String preprocessedText = deidentificationRegex.compositeRegex(text, dateOffset, blackListMap, patientName, patientPHI);
                java.util.Date endDateRegex = new java.util.Date();
                MedicalRecordWrapper record = new MedicalRecordWrapper(id, note_id, text, preprocessedText, null, dateOffset, patientName, patientPHI);
                long msecs = SimpleIRUtilities.getElapsedTimeMilliseconds(startDateRegex, endDateRegex);
                record.setMillisecondsRegex(msecs);
                RegexTotalTime = RegexTotalTime + msecs;
                
                if (opts.testMode()) {
                    System.out.println("***TEST MODE: After PreProcessing");
                    System.out.println(preprocessedText);
                }

                newT.getRecordList().add(record);

                if (newT.getRecordList().size() >= opts.getRecordsPerThread()) {
                    if (threadList.size() >= opts.getNthreads()) {
                        DeIdentificationThread t = threadList.remove(0);
                        try {
                            t.join();
                            if (opts.isUpdateOnly()) {
                                connection.updateDeidRecord(updateSql, t.getRecordList());
                            } else {
                                connection.insertDeidRecord(opts.getDeidnotestablename(), t.getRecordList());
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            return;
                        }
                    }

                    threadList.add(newT);
                    newT.start();
                    newT = new DeIdentificationThread(namedEntityRecognition);
                }
                if (results.getRow()% logFile.getRecordsThreshold() == 0) {
                	logFile.updateStatus(startDate,dbFinishDate, results.getRow(),RegexTotalTime,EntityRecTotalTime, false);	//output current status
                }
                totalRows = results.getRow();
                
            }
            
            // process left-overs
            if (newT.getRecordList().size() > 0) {

                threadList.add(newT);
                newT.start();
            }

            // wait for processing to finish
            try {
                for (DeIdentificationThread t : threadList) {
                    t.join();
                    if (opts.isUpdateOnly()) {
                        connection.updateDeidRecord(updateSql, t.getRecordList());
                    } else {
                        connection.insertDeidRecord(opts.getDeidnotestablename(), t.getRecordList());
                    }
                    //connection.insertDeidRecord(opts.getDeidnotestablename(), t.getRecordList());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
            
            results.close();
            
        } catch (SQLException e) {
            System.out.println("Could not execute query: " + selectSql);
            e.printStackTrace();
        }

        try {
            connection.close();
        } catch (Exception e) {
            System.out.println("2: Could not connect to the database dburl: " + opts.getDburl()
                    + ", login: " + opts.getLogin() + ", password: " + opts.getPassword());
            e.printStackTrace();
        }
    	logFile.updateStatus(startDate,dbFinishDate, totalRows+1 ,RegexTotalTime,EntityRecTotalTime, true);	//output summary

        

    }

    static String readFile(File file, Charset encoding) throws IOException {

        String path = file.getAbsolutePath();
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return encoding.decode(ByteBuffer.wrap(encoded)).toString();
    }

    static String[] loadFileList(File file) throws IOException {

        List<String> list = new ArrayList<String>();
        BufferedReader in = new BufferedReader(new FileReader(file));
        String str;
        while ((str = in.readLine()) != null) {
            String s = str.trim();
            if (s.length() > 0) {
                s = str.toLowerCase(); // normalize to upper case
                list.add(s);
            }
        }
        return list.toArray(new String[list.size()]);
    }
    
    /**
	 * @param ResultSet - query with PHI 
	 */
    static ArrayList<String> findPhi(ResultSet results)  throws SQLException  {
    	ArrayList<String> patientPHI = new ArrayList<String>();
    	for (int i=NumColWithoutPHI+1;i <results.getMetaData().getColumnCount();i++  )	//all columns after patients names are treated as PHI
    		patientPHI.add(results.getString(i)) ;
    	return patientPHI;
    }
    
    
    
    
}
