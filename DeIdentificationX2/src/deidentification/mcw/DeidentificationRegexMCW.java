/**
 * 
 */
package deidentification.mcw;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

/**
 * @author jayurbain
 *
 */
public class DeidentificationRegexMCW implements deidentification.DeidentificationRegex{
	public final static int age = 89;

	/**
	 * @param String - text to convert
	 */
	private static final int PHINum = 10;
	private static final String dateRegex1 = "\\b([0-9]{1,2})[/\\-]([0-9]{4})\\b"; // month/year
//	private static final String dateRegex2 = "(\\s+|^)([0-9]{1,2})[/\\-]([0-9]{1,2})"; // month/day
//	private static final String dateRegex2 = "([0-9]{1,2})[/\\-]([0-9]{1,2})"; // month/day
	private static final String dateRegex2 = "\\b(0?[1-9]|1[0-2])\\s?[/]\\s?(0?[1-9]|[1-2][0-9]|3[0-1])\\b"; // month/day
	private static final String dateRegex3 = "\\b([0-9]{1,2})\\s?[/\\-]\\s?([0-9]{1,2})\\s?[/\\-]\\s*([0-9]{1,4})\\b"; // month/day/year - run before month/day
	private static final String dateRegexMDYWithSpace = "\\b([0-9]{1,2})\\s?[/\\-\\s]\\s?([0-9]{1,2})\\s?[/\\-\\s]\\s?([0-9]{1,4})\\b"; // month/day/year - run after month/day/year
//	private static final String dateRegex33= "\\b([0-9]{1,2})[\\-]([0-9]{1,2})[\\-]\\s*([0-9]{1,4})\\b"; // month/day/year - run before month/day
//	private static final String dateRegex4 = "([ADFJMNOS]\\w*)\\s+([0-9]{1,2})(th|TH|nd|ND){0,1},{0,1}\\s+([0-9]{4})";
	private static final String dateRegex4 = "\\b([AaDdFfJjMmNnOoSs]\\w*)\\s*\\.?\\s+([0-9]{0,2})\\s{0,1}(th|TH|nd|ND){0,1}\\s*,{0,1}\\s+([0-9]{4})\\b";
	private static final String dateRegex5 = "\\b([AaDdFfJjMmNnOoSs]\\w*)\\s*\\.?(\\s+of){0,1}\\s+([0-9]{4})\\b";
	private static final String dateRegex6 = "\\b([AaDdFfJjMmNnOoSs]\\w*)\\s*\\.?\\s+([0-9]{0,2})\\s*(th|TH|nd|ND){0,1},{0,1}\\b";
	private static final String dateRegexMDYwithPeriod = "\\b([0-9]{1,2})\\.\\s?(0?[1-9]|[1-2][0-9]|3[0-1])\\.\\s?([0-9]{2,4})\\b";
	private static final String dateRegexMYwithApos = "\\b(0?[1-9]|[1-2][0-9]|3[0-1])/'([0-9]{2})\\b";
//	private static final String phoneNumberRegex = "\\({0,1}([0-9]{3})\\){0,1}[\\-|\\s|\\.]{0,1}([0-9]{3})[\\-|\\s|\\.]{0,1}([0-9]{4})";
	private static final String phoneNumberRegex = "(1[\\-|\\.]){0,1}\\({0,1}([0-9]{3})\\){0,1}[\\-|\\s|\\.]{0,1}([0-9]{3})[\\-|\\s|\\.]{0,1}([0-9]{4})\\b";
//	private static final String phoneNumberRegex2 = "([0-9]{3})[\\-|\\s|\\.]{0,1}([0-9]{4})";
//	private static final String phoneNumberRegex2 = "\\b([0-9]{3})[\\-|\\.]{0,1}([0-9]{4})\\b";
	private static final String phoneNumberRegex2 = "\\b([0-9]{3})[\\-|\\.]{0,1}([0-9]{4})\\b";
	private static final String AgeGreater89 = "\\b(?i)age(:|\\s*){0,3}((9[0-9])|1[0-9][0-9]){1}\\b";	//Age: (>89)
	private static final String AgeGreater89_2 = "\\b(?i)(9[0-9]|1[0-9][0-9]){1}(:|\\s*){0,1}(yo|y. o|years|year|y. o .){1}\\b";	//Age: (>89)

//	private static final String phoneNumberRegexOfficial = "^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:\\(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\\s*\\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+))?$";

	// [-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}
	private static final String emailRegex = "[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\\.[a-zA-Z]{2,4}";

	// ^\d{5}(?:[-\s]\d{4})?$
//	private static final String zipRegex = "(\\s+|^)\\d{5}(?:[-\\s]\\d{4})?$";
	private static final String zipRegex = "\\b\\d{5}((-)\\d{4})?\\b";
	
//	private static final String addressRegex = "(^|\\s+)(\\d+)(\\s+)((?i)[NSEW](?i-))\\.{0,1}(\\w+)(\\s+|$)";
//	private static final String addressRegex = "(^|\\s+)(\\d+)(\\s+)((?i)[NSEW]{1}(?i-))\\.{0,1}\\s+(\\w+)";
	private static final String addressRegex = "(\\d+)(\\s+)((?i)[NSEW]{1}(?i-))\\.{0,1}\\s+(\\w+)";
	
	private static Pattern datePattern1 = Pattern.compile(dateRegex1);
	private static Pattern datePattern2 = Pattern.compile(dateRegex2);
	private static Pattern datePattern3 = Pattern.compile(dateRegex3);
	private static Pattern datePatternMDYWithSpace = Pattern.compile(dateRegexMDYWithSpace);
	private static Pattern datePattern4 = Pattern.compile(dateRegex4);
	private static Pattern datePattern5 = Pattern.compile(dateRegex5);
	private static Pattern datePattern6 = Pattern.compile(dateRegex6);
	private static Pattern datePatternMDYwithPeriod = Pattern.compile(dateRegexMDYwithPeriod);
	private static Pattern datePatternMYwithApos = Pattern.compile(dateRegexMYwithApos);
	private static Pattern phoneNumberPattern = Pattern.compile(phoneNumberRegex);
	private static Pattern phoneNumberPattern2 = Pattern.compile(phoneNumberRegex2);
	private static Pattern AgeGreater89Pattern = Pattern.compile(AgeGreater89);
	private static Pattern AgeGreater89Pattern2 = Pattern.compile(AgeGreater89_2);

	private static Pattern emailPattern = Pattern.compile(emailRegex);
	
	private static Pattern zipPattern = Pattern.compile(zipRegex);
	
	private static Pattern addressPattern = Pattern.compile(addressRegex);
	
//	private final static String mixedCaseAlphaNumericRegex1 = "([a-z|A-Z])([0-9])"; // would separate A1C
//	private final static String mixedCaseAlphaNumericRegex2 = "([0-9])([a-z|A-Z])"; v// would separate 3rd
//	private final static String mixedCaseAlphaNumericRegex2 = "(\\s+|^)([0-9]+)([A-Z])";
	private final static String mixedCaseAlphaNumericRegex2 = "([0-9])([a-z|A-Z])";
	private final static String mixedCaseAlphaNumericRegex3 = "([a-z|A-Z])([0-9])";
	private final static String camelCaseRegex1 = "([A-Z|a-z])([A-Z][a-z]+)";
	private final static String [] monthArrayLong = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
	private final static String [] monthArrayShort = {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"};
	
	
	public static String mixedCaseAlphaNumericTransition(String s) {
//		return s.replaceAll(mixedCaseAlphaNumericRegex1,replacement).replaceAll(mixedCaseAlphaNumericRegex2,replacement).replaceAll(mixedCaseAlphaNumericRegex3,replacement);
		return s.replaceAll(mixedCaseAlphaNumericRegex2,"$1 $2").replaceAll(mixedCaseAlphaNumericRegex3,"$1 $2");
	}
	
	public static String camelCaseAlphaTransition(String s) {
//		return s.replaceAll(mixedCaseAlphaNumericRegex1,replacement).replaceAll(mixedCaseAlphaNumericRegex2,replacement).replaceAll(mixedCaseAlphaNumericRegex3,replacement);
		return s.replaceAll(camelCaseRegex1,"$1 $2");
	}
	
	
	
	/**
	 * @param String - text to convert
	 */
	public static String ageGreater89Regex(String s) {

		Matcher matcher = AgeGreater89Pattern.matcher(s);
		StringBuffer bufStr = new StringBuffer();
		while( matcher.find() ) {
			matcher.appendReplacement(bufStr, "Age: [xxx] ");
		}
		matcher.appendTail(bufStr);		
		String s2 = bufStr.toString();		
		return s2;
	}
	
	/**
	 * @param String - text to convert
	 */
	public static String ageGreater89Regex2(String s) {

		Matcher matcher = AgeGreater89Pattern2.matcher(s);
		StringBuffer bufStr = new StringBuffer();
		while( matcher.find() ) {
			matcher.appendReplacement(bufStr, "[xxx] years ");
		}
		matcher.appendTail(bufStr);		
		String s2 = bufStr.toString();		
		return s2;
	}
	
	/**
	 * @param String - text to convert
	 */
	public static String phoneNumberRegex(String s) {

		Matcher matcher = phoneNumberPattern.matcher(s);
		StringBuffer bufStr = new StringBuffer();
		while( matcher.find() ) {
			matcher.appendReplacement(bufStr, " [xxx_xxx_xxxx] ");
		}
		matcher.appendTail(bufStr);		
		String s2 = bufStr.toString();		
		return s2;
	}
	
	/**
	 * @param String - text to convert
	 */
	public static String phoneNumberRegex2(String s) {

		Matcher matcher = phoneNumberPattern2.matcher(s);
		StringBuffer bufStr = new StringBuffer();
		while( matcher.find() ) {
			matcher.appendReplacement(bufStr, " [xxx_xxxx] ");
		}
		matcher.appendTail(bufStr);
		return bufStr.toString();
	}
	
	/**
	 * @param String - text to convert
	 */
	public static String emailRegex(String s) {

		Matcher matcher = emailPattern.matcher(s);
		StringBuffer bufStr = new StringBuffer();
		while( matcher.find() ) {
			matcher.appendReplacement(bufStr, " [xxx@xxx.xxx] ");
		}
		matcher.appendTail(bufStr);
		return bufStr.toString();
	}
	
	/**
	 * @param String - text to convert
	 */
	public static String zipRegex(String s) {

		Matcher matcher = zipPattern.matcher(s);
		StringBuffer bufStr = new StringBuffer();
		while( matcher.find() ) {
			matcher.appendReplacement(bufStr, " [xxxxx] ");
		}
		matcher.appendTail(bufStr);
		return bufStr.toString();
	}
	
	/**
	 * @param String - text to convert
	 */
	public static String addressRegex(String s) {
		// (\d+)(\s+)((?i)[NSEW]{1}(?i-))\.{0,1}\s+(\w+)
		Matcher matcher = addressPattern.matcher(s);
		StringBuffer bufStr = new StringBuffer();
		while( matcher.find() ) {
			matcher.appendReplacement(bufStr, " [xxxxx x. xxxxx] ");
		}
		matcher.appendTail(bufStr);
		return bufStr.toString();
	}

	/**
	 * @param String - text to convert
	 */
	public static String dateRegex(String s, int dayOffset) {

//		System.out.println("Before date: " + s );
		Calendar cal = null;
		StringBuffer bufStr = new StringBuffer();
		
//		private static final String dateRegex1 = "\\b([0-9]{1,2})[/\\-]([0-9]{4})\\b"; // month/year 
//		private static final String dateRegex2 = "\\b([0-9]{1,2})[/\\-]([0-9]{1,2})\\b"; // month/day
//		private static final String dateRegex3 = "\\b([0-9]{1,2})[/]([0-9]{1,2})[/]\\s*([0-9]{1,4})\\b"; // month/day/year - run before month/day
//		private static final String dateRegex33= "\\b([0-9]{1,2})[\\-]([0-9]{1,2})[\\-]\\s*([0-9]{1,4})\\b"; // month/day/year - run before month/day
//		private static final String dateRegex4 = "\\b([ADFJMNOS]\\w*)\\s+([0-9]{0,2})(th|TH|nd|ND){0,1},{0,1}\\s+([0-9]{4})\\b";
//		private static final String dateRegex5 = "\\b([ADFJMNOS]\\w*)\\s+([0-9]{4})\\b";
//		private static final String dateRegex6 = "\\b([ADFJMNOS]\\w*)\\s+([0-9]{0,2})(th|TH|nd|ND){0,1},{0,1}\\b";
	
		Matcher matcher3 = datePattern3.matcher(s);
		bufStr = new StringBuffer();
		while( matcher3.find() ) {
			//		 	 System.out.println( matcher3.group());
			int month = Integer.parseInt(matcher3.group(1));
			int day = Integer.parseInt(matcher3.group(2));
			int year = Integer.parseInt(matcher3.group(3));
			String zeropad="";
			if( year < 10) {
				zeropad="0";
			}
//			if( month>= 1 && month <= 12 && year > 1900 && year < 2100)  {
			if( month>= 1 && month <= 12 && year > 0 && year < 2100)  {
				cal =  createConvertedDate(month, day, year, dayOffset);
				String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + zeropad + (cal.get(Calendar.YEAR)) + "] ";
	//			newDateString = "xx_xx_xxxx";
				if( month > 12) { // not a date
					newDateString = " [" + matcher3.group(1) + "/" + matcher3.group(2) + "/" + matcher3.group(3) + "] ";
				}
				matcher3.appendReplacement(bufStr, newDateString);
			}
		}
		matcher3.appendTail(bufStr);


		Matcher matcherMDYWithSpace = datePatternMDYWithSpace.matcher(bufStr.toString());
		bufStr = new StringBuffer();
		while( matcherMDYWithSpace.find() ) {
			//		 	 System.out.println( matcher3.group());
			int month = Integer.parseInt(matcherMDYWithSpace.group(1));
			int day = Integer.parseInt(matcherMDYWithSpace.group(2));
			int year = Integer.parseInt(matcherMDYWithSpace.group(3));
			String zeropad="";
			if( year < 10) {
				zeropad="0";
			}
//			if( month>= 1 && month <= 12 && year > 1900 && year < 2100)  {
			if( month>= 1 && month <= 12 && year > 0 && year < 2100)  {
				cal =  createConvertedDate(month, day, year, dayOffset);
				String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + zeropad + (cal.get(Calendar.YEAR)) + "] ";
				//			newDateString = "xx_xx_xxxx";
				if( month > 12) { // not a date
					newDateString = " [" + matcherMDYWithSpace.group(1) + "/" + matcherMDYWithSpace.group(2) + "/" + matcherMDYWithSpace.group(3) + "] ";
				}
				matcherMDYWithSpace.appendReplacement(bufStr, newDateString);
			}
		}
		matcherMDYWithSpace.appendTail(bufStr);


	 /* Matcher matcher33 = datePattern33.matcher(bufStr.toString());
		bufStr = new StringBuffer();
		while( matcher33.find() ) {
			//		 	 System.out.println( matcher33.group());
			int month = Integer.parseInt(matcher33.group(1));
			int day = Integer.parseInt(matcher33.group(2));
			int year = Integer.parseInt(matcher33.group(3));
			String zeropad="";
			if( year < 10) {
				zeropad="0";
			}
//			if( month>= 1 && month <= 12 && ((year > 1900 && year < 2100) || (year > 0 && year < 100)) )  {
			if( month>= 1 && month <= 12 && year > 0 && year < 2100)  {
				cal =  createConvertedDate(month, day, year, dayOffset);
				String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + zeropad + (cal.get(Calendar.YEAR)) + "] ";
	//			newDateString = "xx_xx_xxxx";
				if( month > 12) { // not a date
					newDateString = " [" + matcher33.group(1) + "/" + matcher33.group(2) + "/" + matcher33.group(3) + "] ";
				}
				matcher33.appendReplacement(bufStr, newDateString);
			}
		}
		matcher33.appendTail(bufStr); */
		
		
		Matcher matcher4 = datePattern4.matcher(bufStr.toString());
		bufStr = new StringBuffer();
		while( matcher4.find() ) {
			//		 	 System.out.println( matcher3.group());
			try {
				int month = convertDateNameToNumber(matcher4.group(1)); // String month
				if( month <= 0 ) {
					continue;
				}
				String dayStr = matcher4.group(2);
				if( !(dayStr != null && dayStr.length() > 0 ) ) {
					continue;
				}
				int day = Integer.parseInt(dayStr);
				int year = Integer.parseInt(matcher4.group(4));
				String zeropad="";
				if( year < 10) {
					zeropad="0";
				}
				if( month>= 1 && month <= 12 && year > 1900 && year < 2100)  {
					cal =  createConvertedDate(month, day, year, dayOffset);
					String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + zeropad + (cal.get(Calendar.YEAR)) + "] ";
	//			newDateString = "xx_xx_xx";
					matcher4.appendReplacement(bufStr, newDateString);
				}
			} catch (NumberFormatException e) {
//				System.out.println("matcher4.group(1):"+matcher4.group(1) + "; matcher4.group(2):" + matcher4.group(2) + "; matcher4.group(4): "+ matcher4.group(4));
				e.printStackTrace();
			}
		}
		matcher4.appendTail(bufStr);
		
		Matcher matcher5 = datePattern5.matcher(bufStr.toString());
		bufStr = new StringBuffer();
		while( matcher5.find() ) {
			//		 	 System.out.println( matcher3.group());
			int month = convertDateNameToNumber(matcher5.group(1)); // String month
			int day = 15;
			int year = Integer.parseInt(matcher5.group(3));
			String zeropad="";
			if( year < 10) {
				zeropad="0";
			}
			if( month>= 1 && month <= 12 )  {
				cal =  createConvertedDate(month, day, year, dayOffset);
				String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + zeropad + (cal.get(Calendar.YEAR)) + "] ";
	//			newDateString = "xx_xx_xx";
				matcher5.appendReplacement(bufStr, newDateString);
			}
		}
		matcher5.appendTail(bufStr);
		
		Matcher matcher6 = datePattern6.matcher(bufStr.toString());
		bufStr = new StringBuffer();
		while( matcher6.find() ) {
			//		 	 System.out.println( matcher3.group());
			int month = convertDateNameToNumber(matcher6.group(1)); // String month
			if( month <= 0 ) {
				continue;
			}
			String dayString = matcher6.group(2);
			int day = 0;
			try {
				day = Integer.parseInt(matcher6.group(2));
			} catch (NumberFormatException e) {
				continue;
			}
			int year = 0;
			String zeropad="";
			if( month>= 1 && month <= 12 )  {
				cal =  createConvertedDate(month, day, year, dayOffset);
				String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + zeropad + (cal.get(Calendar.YEAR)) + "] ";
	//			newDateString = "xx_xx_xx";
				matcher6.appendReplacement(bufStr, newDateString);
			}
		}
		matcher6.appendTail(bufStr);
		
//		private static final String dateRegex1 = "([0-9]{1,2})[/\\-]([0-9]{4})"; // month/year 
		Matcher matcher1 = datePattern1.matcher(bufStr.toString());
		bufStr = new StringBuffer();
		while( matcher1.find() ) {
			//		 	 System.out.println( matcher1.group());
			int month = Integer.parseInt(matcher1.group(1));
			int day = 15; // arbitrary date see for obfuscation
			int year = Integer.parseInt(matcher1.group(2));
			// restrict conversion
			if( month>= 1 && month <= 12 && year > 1900 && year < 2100)  {
				cal =  createConvertedDate(month, day, year, dayOffset);
				String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + (cal.get(Calendar.YEAR)) + "] ";
//				newDateString = "xx_xxxx";
				matcher1.appendReplacement(bufStr, newDateString);
			}
		}
		matcher1.appendTail(bufStr);

		Matcher matcher2 = datePattern2.matcher(bufStr.toString());
		bufStr = new StringBuffer();
		while( matcher2.find() ) {
			int month = Integer.parseInt(matcher2.group(1));
			int day = Integer.parseInt(matcher2.group(2));
			int year = 0;

			cal =  createConvertedDate(month, day, year, dayOffset);
			String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "] ";
			matcher2.appendReplacement(bufStr, newDateString);
		}
		matcher2.appendTail(bufStr);

		Matcher matcherMDYwithPeriod = datePatternMDYwithPeriod.matcher(bufStr.toString());
		bufStr = new StringBuffer();
		while(matcherMDYwithPeriod.find()){
			int month = Integer.parseInt(matcherMDYwithPeriod.group(1));
			int day = Integer.parseInt(matcherMDYwithPeriod.group(2));
			int year = Integer.parseInt(matcherMDYwithPeriod.group(3));

			cal = createConvertedDate(month, day, year, dayOffset);
			String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + (cal.get(Calendar.YEAR)) + "] ";
			matcherMDYwithPeriod.appendReplacement(bufStr, newDateString);
		}
		matcherMDYwithPeriod.appendTail(bufStr);

		Matcher matcherMYwithApos = datePatternMYwithApos.matcher(bufStr.toString());
		bufStr = new StringBuffer();
		while(matcherMYwithApos.find()){
			int month = Integer.parseInt(matcherMYwithApos.group(1));
			int day = 15;
			int year = Integer.parseInt(matcherMYwithApos.group(2));

			cal = createConvertedDate(month, day, year, dayOffset);
			String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + (cal.get(Calendar.YEAR)) + "] ";
			matcherMYwithApos.appendReplacement(bufStr, newDateString);
		}
		matcherMYwithApos.appendTail(bufStr);

		//		String s2 = s.replaceAll(dateRegex3, "$1|$2|$3").replaceAll(dateRegex2, "$1|$2");
//		System.out.println("After date: " + bufStr.toString() );
		return bufStr.toString();
	}
	
	public boolean validDate(int month, int day, int year) {
		boolean valid = true;
		if( month == 0 || month<0 || month>12 || year<1900 || year>2100 ) {
			valid= false;
		}
		return valid;
	}
	
	/**
	 * Adjust given month, day, and optional year to date with dayOffset
	 * dayOffset could be positive or negative
	 * 0 for year means no year available, so use current year
	 * 
	 *  @param String - text to convert
	 */
	public static Calendar createConvertedDateOld(int month, int day, int year, int dayOffset) {

		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.MONTH, month-1);
		cal.set(Calendar.DAY_OF_MONTH, day);
		if( year > 0 ) {
			cal.set(Calendar.YEAR, year);
		}

		// Get the number of days in that month
		int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		if( day + dayOffset > daysInMonth ) { 
			month = month+1;
			cal.set(Calendar.MONTH, (month-1)); // Java counts months starting at 0
			cal.set(Calendar.DAY_OF_MONTH, day + dayOffset - daysInMonth);
		}
		else if( day + dayOffset < 1 ) {
			month = month-1;
			cal.set(Calendar.MONTH, (month-1));
			daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH); // update days in month
			cal.set(Calendar.DAY_OF_MONTH, daysInMonth + dayOffset + day);
		}
		else {
			cal.set(Calendar.DAY_OF_MONTH, day+dayOffset);
		}
		
		// Get the number of months in year
		if( month > 12 ) { 
			cal.set(Calendar.YEAR, year+1);
			cal.set(Calendar.MONTH, (1) -1);
		}
		else if( month < 1 ) {
			cal.set(Calendar.YEAR, year-1);
			cal.set(Calendar.MONTH, (12) -1);
		}
		
		return cal;
	}
	
	/**

	* Adjust given month, day, and optional year to date with dayOffset

	* dayOffset could be positive or negative

	* 0 for year means no year available, so use current year

	* 

	*  @param String - text to convert

	*/

	public static Calendar createConvertedDate(int month, int day, int year, int dayOffset) {

		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.MONTH, month-1);
		cal.set(Calendar.DAY_OF_MONTH, day);
		if( year > 0 ) {
			cal.set(Calendar.YEAR, year);
		}

		Date d = DateUtils.addDays(cal.getTime(), dayOffset);
//		String newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + (cal.get(Calendar.YEAR)) + "] ";
//		System.out.println(newDateString);
		cal.setTime(d);
//		newDateString = " [" + (cal.get(Calendar.MONTH)+1) + "_" + (cal.get(Calendar.DAY_OF_MONTH)) + "_" + (cal.get(Calendar.YEAR)) + "] ";
//		System.out.println(newDateString);
		return cal;
	}

	public static int convertDateNameToNumber(String s) {
		
		int i=0;
		String ss = s.toLowerCase();
		for( i=0; i< monthArrayLong.length; i++) {
			if( ss.equals(monthArrayLong[i])) {
				return i+1;
			}
		}
		for( i=0; i< monthArrayShort.length; i++) {
			if( ss.startsWith(monthArrayShort[i])) {
				return i+1;
			}
		}
		return 0;
	}

	/**
	 * De-identify MRN here, as ID entities was turned off in MIST since 
	 * their algorithm considers some procedures words like Spirometry and Pap as identifiers.
	 * 
	 * Note: An MRN can come as 01234567 or even without leading zeros. It can also
	 * appear as 01-23-45-67.
	 * 
	 * @param String - text to convert
	 */
//	private static final String patientidRegex1 = "([0-9]{7,8})";
//	private static final String patientidRegex1 = "\\b([0-9]{6,9})\\b";
	private static final String patientidRegex1 = "([0-9]{6,15})";
	private static final String patientidRegex2 = "([0-9]{1,2})\\-([0-9]{1,2})\\-([0-9]{1,2})\\-([0-9]{1,2})\\-([0-9]{1,2})";

	public static String mrnRegex(String s) {

		String s1 = s.replaceAll(patientidRegex1, " [xxxxxxxx] ");
		String s2 = s1.replaceAll(patientidRegex2, " [xx-xx-xx-xx-xx] ");
		return s2;
	}
	
	//public static String claimNumberRegex = "^(?=.*[a-zA-Z])(?=.*[0-9])$";
//	public static String claimNumberRegex = "^(?=.*[a-zA-Z].*)([a-zA-Z0-9]{6,12})$";
//	public static String claimNumberRegex = "^\\d*[a-zA-Z][a-zA-Z0-9]*$";
//	public static String claimNumberRegex1 = "\\b[a-zA-Z][0-9][a-zA-Z0-9-]*\\b";
//	public static String claimNumberRegex1 = "(([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[a-zA-Z0-9-]*){2,}";
	public static String claimNumberRegex1 = "\\b(([0-9]+[a-zA-Z]+)|([a-zA-Z]+[0-9]+))[a-zA-Z0-9-]*\\b";
//	public static String claimNumberRegex1 = "^(?=.*([0-9][a-zA-Z])).{5,20}$";
//	public static String claimNumberRegex1 = "\\b(?=.*(\\d|[a-zA-Z]))[a-zA-Z]{5,20}\\b";
//	public static String claimNumberRegex1 = "\\b(?=\\d)(?=[a-zA-Z])[a-zA-Z0-9-]*\\b";
	
	public static String claimNumberRegex(String s) {

		String s1 = s.replaceAll(claimNumberRegex1, " [xxxxxxxx] ");
		return s1;
	}
	
	/**
	 * Convert selected procedure names to ALL CAPS prior to running
	 * through MIST deidentification as the following examples shows
	 * that MIST identifies certain procedures as identifiers or names. 
	 * If we first convert to ALL CAPS it will not try to deid.
	 * Example:
	 * 126	10534	Spirometry 6/10/04  	Zeeevudjpc 9/2314
	 * 
	 * @param String - text to convert
	 */
	private static String [] procedureAllCapsArray = {"COLONOSCOPY", "PAP"};
	public static String procedureAllCaps(String s) {

		for(int i=0; i<procedureAllCapsArray.length; i++) {
			s = s.replaceAll("(?i)"+procedureAllCapsArray[i], procedureAllCapsArray[i] );
		}
		return s;
	}
	
	public static String whiteList(String s, String [] whiteListArray) {

		for(int i=0; i<whiteListArray.length; i++) {
			s = s.replaceAll("(?i)"+whiteListArray[i], whiteListArray[i] );
		}
		return s;
	}
	
	public static boolean inWordList(String s, String [] listArray) {

		for(int i=0; i<listArray.length; i++) {
			if( s.toLowerCase().equals( listArray[i] ) ) {
				return true;
			}
		}
		return false;
	}
	
	public static String blackList(String s, Map<String, String> blackListMap) {
		
		Set<String> set = blackListMap.keySet();
		String sLowerCase = s.toLowerCase();
		for(String key : set) {
			if (sLowerCase.contains(key)) { 		//check to see if the text contains the keyword 
				s = s.replaceAll("\\b(?i)"+key+"\\b", " [XXXXX] " );
			}

	
		}
		return s;
	}
	
	/**
	 * Takes a String s (patient record), and a String array for the patientName 
		[Urbain, Jay F; Jay; F; Urbain). Replaces words in name with "[PATIENT]".
		Note: it handles the middle initial name. 
	 * @param s - text to be converted
	 * @param ListPatientName - patient list names [fullname, first, middle, last]
	 * @return s
	 */
	public static String findMiddleInitial(String s, String [] ListPatientName) {
		
		ArrayList<String> fullname = new ArrayList<>();
		String middle = ListPatientName[1]!= null && ListPatientName[1].length() > 1  ?  "(" +ListPatientName[1]+"|"+ ListPatientName[1].substring(0,1)+")" : ListPatientName[1]; //consider both full and initial middle name 
		fullname.add(ListPatientName[2]+"(\\.|\\s+|,){0,2}"+ListPatientName[0] + "(\\.|\\s+|,){0,1}" +middle); // Urbain, Jay Y. 
		fullname.add(ListPatientName[0]+"(\\.|\\s+|,){0,2}"+ListPatientName[2] + "(\\.|\\s+|,){0,1}" +middle); // Jay, Urbain Y. 
		fullname.add(ListPatientName[0]+"((\\.|\\s+|,){0,1})"+middle+"{0,1}"+"((\\.|\\s+|,){0,2})"+ListPatientName[2]); // Jay Y. Urbain 
		fullname.add(ListPatientName[2]+"((\\.|\\s+|,){0,1})"+middle+"{0,1}"+"((\\.|\\s+|,){0,2})"+ListPatientName[0]); // Urbain Y. Jay
		
			
		for (int i=0;i<fullname.size();i++) {
			try {
			Pattern PHIPattern = Pattern.compile("\\b(?i)"+fullname.get(i)+"\\b");
			Matcher matcher = PHIPattern.matcher(s);
			StringBuffer bufStr = new StringBuffer();
			while( matcher.find() ) {
				matcher.appendReplacement(bufStr, " [PATIENT] [PATIENT] ");
			}
			matcher.appendTail(bufStr);
			s = bufStr.toString();}
			catch(Exception e) { 
				//e.printStackTrace();
				continue;}
			
		}
		return s;
	
	}
	/**
	 * Takes a String s (patient record), and a String array for the patientName 
		(Urbain, Jay F; Jay Urbain; Jay F Urbain). Replaces words in name with "[PATIENT]".
		Note: Care had to be taken when writing this method to ensure words in name
		are on word boundaries, and the middle initial, if provided, is within the position
		range of the name. Otherwise it was capturing single letter abbreviations used
		in records.
	 * @param s
	 * @param patientName
	 * @return
	 */
	
	public static String patientNameList(String s, String [] PatientNameList) {
		
		String patientName = PatientNameList[0];
		
		if( patientName == null || patientName.length() == 0  ) {
			return s;
		}
		
		//handle cases where the middle initial is provided (e.g. Jay Y. Urbain; Jay Y Urbain)
		if (PatientNameList[1] != null && PatientNameList[3] != null) {
			s = findMiddleInitial(s,  Arrays.copyOfRange(PatientNameList, 1, 4));}
		
		
		int maxPos = Integer.MIN_VALUE;
		int minPos = Integer.MAX_VALUE;
		String sOriginal = s;
		
		String [] patientNameArray = patientName.split("[\\s|\\.|,]+");
		List<String> patientNameList = new ArrayList<String>(Arrays.asList(patientNameArray));
			
		// find all occurrences forward of name words > 1 character in length
		// save max, min positions for name range to limit single character middle initial replacements 
		for(String name : patientNameList) {
			StringBuffer bufStr = new StringBuffer();
			if( name.length() > 1 ) {
				String regex = "\\b(?i)"+name+"\\b";
			    Pattern pattern = null;
			    Matcher matcher = null;
				try {
					pattern = Pattern.compile(regex);
				    matcher = pattern.matcher(s);
				    while( matcher.find() ) {
					    if( matcher.start() > maxPos) {
					    	maxPos = matcher.start();
					    }
					    if( matcher.start() < minPos) {
					    	minPos = matcher.start();
					    }
					    matcher.appendReplacement(bufStr, " [PATIENT] ");
				    }
				    matcher.appendTail(bufStr);
				    s = bufStr.toString();
				} catch (Exception e) {
					System.out.println("regex: " + regex);
					e.printStackTrace();
				}	
			}
			
		}
		
		// second pass to take char of middle initials iff with full name range
		for(String name : patientNameList) {
			StringBuffer bufStr = new StringBuffer();
			if( !sOriginal.equals(s) && name.length() == 1 && patientNameList.size() > 2 ) {
				String regex = "\\b(?i)"+name+"\\b";
			    Pattern pattern = null;
			    Matcher matcher = null;
				try {
				    pattern = Pattern.compile(regex);
				    matcher = pattern.matcher(s);
				    while( matcher.find() ) {
					    if( matcher.start() > minPos && matcher.end() < maxPos) {
					    	matcher.appendReplacement(bufStr, " [PATIENT] ");
					    	break; // "should" only be one
					    }
				    }
				    matcher.appendTail(bufStr);
				    s = bufStr.toString();
				} catch (Exception e) {
					System.out.println("regex: " + regex);
					e.printStackTrace();
				}
			}
			
		}
		
		return s;
	}
	
//	// Simplification and ~x10 speed up of original patientNameList() method
//	public static String patientNameList(String s, String patientName) {
//
//		if( patientName == null || patientName.length() == 0 ) {
//			return s;
//		}
//		String [] patientNameArray = patientName.split("[\\s|\\.|,]+");
//		List<String> patientNameList = new ArrayList<String>(Arrays.asList(patientNameArray));
//					
//		for(String name : patientNameList) {
//			if( name.length() > 2 ) {
//				s = StringUtils.replace(s, name.toLowerCase(), "[PATIENT]");
//			}
//		}
//		return s;
//	}
	
//////////////////////////////////////////////////////////////////
	
	/**
	 * Takes patient PHI array, and a String number. Enumerate different format that the PHI number can be written as (considering different separators).
	 * For example: phone number written with dashes: [505-780-3333] can be de-identify if it was written as: [5057803333] or [505 780 3333] or [505 780  3333]
					SSN [930-56-3456] can be identified if it was written as [930563456]
	 * @param newPatientPHI - array with patient PHI
	 * @param number - PHI 
	 * @return newPatientPHI
	 */
	public static void findPhiNumbers(ArrayList<String> newPatientPHI, String number) {		
		
		if (number != null && number.indexOf("-")>=0) {
			newPatientPHI.add(number.replaceAll("-", "((\\\\s+|-|\\(|\\)){0,1})"));}

	}
	
	
	/**
	 * Takes patient PHI array, and a String address. Enumerate different format that the PHI can be written as by creating a regexp for the patient address.
	 * @param newPatientPHI - array with patient PHI
	 * @param string - address 
	 * @return newPatientPHI
	 */
	public static void findPhiAddress(ArrayList<String> newPatientPHI, String address) {		
		
		if (address != null) {
			address = address.replaceAll("\\.", "((\\\\\\\\.|\\\\\\\\s+|,){0,1})");
			address = address.replaceAll("\\s", "((\\\\s+){0,1})");
			}
		newPatientPHI.add(address);
	}
	
	
	/**
	 * Takes an array newPatientPHI (patient PHI), the date of the visit, and the patient birth date.
	 * identify patient ages greater than 89, and consider their age as a PHI
	 * @param newPatientPHI - array with patient PHI
	 * @param year_update_st - visit date 
	 * @param year_birth_st
	 * @return newPatientPHI
	 */
	public static void filterAge(ArrayList<String> newPatientPHI, String year_update_st, String year_birth_st) {		
			

				//parse to LocalDate format
				LocalDate year_update = LocalDate.of(Integer.parseInt(year_update_st.substring(6,10)), Integer.parseInt(year_update_st.substring(3,5)), Integer.parseInt(year_update_st.substring(0,2)));	
				LocalDate year_birth = LocalDate.of(Integer.parseInt(year_birth_st.substring(6,10)), Integer.parseInt(year_birth_st.substring(3,5)), Integer.parseInt(year_birth_st.substring(0,2))); 
				LocalDate current =  LocalDate.now();
				
				//treat patient age as PHI if it's greater than 89
				if ( ChronoUnit.YEARS.between(year_birth , current) > age)
					newPatientPHI.add(String.valueOf(ChronoUnit.YEARS.between( year_birth, year_update) ));
					
					
	}
	
	
	/**
	 * process patient PHI, enumerate different format that the PHI can be written as.
	 * @param patientPHI - array with patient PHI
	 * @return newPatientPHI
	 */
	public static ArrayList<String> preProcessPhi(ArrayList<String> patientPHI) {
		if (patientPHI.size() != PHINum)	//not all PHI are provided
				return patientPHI;
		ArrayList<String> newPatientPHI = new ArrayList<String>();
		
		// address 
		findPhiAddress(newPatientPHI,patientPHI.get(0));
		//create different formats for each known PHI identifier
		//zip code: 87122-87128
		String ZIP = patientPHI.get(2);	
		if (ZIP != null && ZIP.indexOf("-")>=0) {
			
			String [] parts = ZIP.split("-");
			if (parts[0].length() == 5)
				newPatientPHI.add(parts[0]);
			if (parts[1].length() == 5)
				newPatientPHI.add(parts[1]);  }
	
		
		// find patient age at the time of visit 
		if (patientPHI.get(9)  != null && patientPHI.get(6) != null){
			filterAge(newPatientPHI,patientPHI.get(9), patientPHI.get(6));}  
		
		
		findPhiNumbers(newPatientPHI,patientPHI.get(3));
		findPhiNumbers(newPatientPHI,patientPHI.get(4));
		findPhiNumbers(newPatientPHI,patientPHI.get(7));
		for (int i= 1; i<patientPHI.size()-1;i++)
		{
			if (i == 6)
				continue;
			if (patientPHI.get(i) != null && patientPHI.get(i).length() >0)
				newPatientPHI.add(patientPHI.get(i));
		}
		
		return newPatientPHI;
	
	}
	
	/**
	 * de-identify patient PHI from text
	 * @param s - text to be DEID
	 * @param patientPHI - array with patient PHI
	 * @return s
	 */
	public static String filterPhi(String s, ArrayList<String> patientPHI) {
		
		if( patientPHI.size() == 0 ) {
			return s;
		}
		
		patientPHI = preProcessPhi(patientPHI);
		for (int i=0;i < patientPHI.size();i++) {
			try {
				Pattern PHIPattern = Pattern.compile("\\b(?i)"+patientPHI.get(i)+"\\b");
				Matcher matcher = PHIPattern.matcher(s);
				StringBuffer bufStr = new StringBuffer();
				while( matcher.find() ) {
					matcher.appendReplacement(bufStr, " [xxxxx] ");
				}
				matcher.appendTail(bufStr);
				s = bufStr.toString();}
			catch(Exception e) { 
				//e.printStackTrace();
				continue;}
		}
		return s;
	}
////////////////////////////////////////////////////

	public String preProcessText(String s){

		s = s.replaceAll("\"", " ");
		s = s.replaceAll("#", " # ");
		s = s.replaceAll("\\.", ". ");
		s = s.replaceAll(":", " : ");
		s = s.replaceAll(",", " , ");
		s = s.replaceAll(";", " ; ");
		s = s.replaceAll("_", " ");
//		s = s.replaceAll("/", " / ");
		s = s.replaceAll("\\s+", " ").trim();
		s = procedureAllCaps(s);

		return s;
	}

	/**
	 * @param String - s, text to convert
	 * @param String - dateStr, date of service
	 * @param String - dateOffset, day offset assigned to this patient for date conversion
	 * 
	 * Place preprocessing regular expression conversions method calls here
	 * Called from deidentification process.
	 * 
	 * Note: Order dependent!
	 * 
	 */

	public String compositeRegex(String s, int dateOffset, Map<String, String> blackListMap, String [] patientName, ArrayList<String> patientPHI) {
		
		s = emailRegex(s);
		if (patientName.length >= 1)
			s = patientNameList(s, patientName);
		s = filterPhi(s, patientPHI);
		s = blackList(s, blackListMap);
		// 1) pre-processing
		s = preProcessText(s);
		
		// 2) regex
//		s = claimNumberRegex(s);
		s = mixedCaseAlphaNumericTransition(s);
		s = blackList(s, blackListMap);
		s = camelCaseAlphaTransition(s);
		s = mrnRegex(s);
		s = phoneNumberRegex(s);
		s = phoneNumberRegex2(s);
		s = dateRegex(s, dateOffset);
		s = addressRegex(s);
		s = zipRegex(s);
		s = ageGreater89Regex(s);
		s = ageGreater89Regex2(s);
		// 3) postprocessing
		s = replaceInvalidCharacterEncoding(s);
		s = s.replaceAll("/", " / ");

//		System.out.println(s);
		return s;
	}
	
	
	/**
	 * @param String - s, text to check for invalid characters
	 * @param String - dateStr, date of service
	 * @param String - dateOffset, day offset assigned to this patient for date conversion
	 * 
	 */
	public static String replaceInvalidCharacterEncoding(String s) {
		
//		System.out.println(s); 
		String sout = s.replaceAll( "([\\ud800-\\udbff\\udc00-\\udfff])", " ");
//		System.out.println(sout); 
		return sout;
	}
	
	public static void main(String[] args) {
		
//		The following are covered:
//			return to work on 7/1/13
//			The patient will follow up in 7-23-13
//
//			I did not cover these, but will:
//			return to work on July 9th 2013
//			return to work on July 18, 2013
//			He may return to work on 7-4, 2013.
		
		DeidentificationRegexMCW deidentificationRegex = new DeidentificationRegexMCW();
		
		String [] whiteList = new String[] {"pap"};
		Map<String, String> blackListMap = new HashMap<String, String>();
//
//
		String sc = "CLAIM # A5U3140 for patient Jay Urbain";
//		sc = "A5U3140";
		String scx = claimNumberRegex(sc);
		
		sc = "Jay Urbain is number 201408150850";
		scx = mrnRegex(sc);
		
		sc = "Weekly Rehab Team Meeting Patient :  [PATIENT]   [PATIENT]  Date of Team Meeting :  [12_23_2]  , 2014 0815   0850 Team Members Present : attending physician , nurse , occupational therapist , speech language therapist and Rehab Counselor   Dr . B Gandhavadi , A . Ihnen ; Jeannie J . , Joan C . , and Sarah S . Notetaker for mtg is Jeannine M Jung , OT Patient and Support System present : Patient and wife :  [PATIENT]  and Brenda Impairments : Impairment of mobility , safety and Activities of Daily Living (AD Ls) due to cognitive deficits , communication deficits , loss of coordination , pain , weakness and balance . DIAGNOSIS : PRINCIPAL DIAGNOSIS : Nontraumatic cerebral hemorrhage . SECONDARY DIAGNOSES : 1 . Hemorrhagic complication from TPA for right ischemic cerebrovascular accident . 2 . Hypertension . 3 . History of psychosis . 4 . Obesity . 5 . Gastric bypass surgery . 6 . Bilateral hip surgery . 7 . Peripheral neuropathy clinically . 8 . History of bilateral carpal tunnel surgery and release Hospital Complications : none (no complications) Associated Injuries : none Functional Independent Measure Flow Sheet FIM Categories Admit Score 12/16 1 st Conf 12/19 2 nd Conf 12/26 3 rd Conf 01/02 4 th Conf Discharge Goal 1 . Bladder Management 2 2 3 4 7 2 . Bowel Management 1 1 2 4 7 3 . Bed/Chair/WC Transfer 1 3 4 5 6 4 . Toilet Transfer 1 1 4 4 6 5 . Tub/Shower Transfer 0 0 0 3 5 6 . Walk/WC 1/2 1/2 2/2 4 5 7 . Stairs 0 0 0 4 6 8 . Eating 5 5 5 6 7 9 . Grooming 3 5 5 6 7 10 . Bathing 3 3 3 4 6 11 . Dressing Upper 3 4 5 5 6 12 . Dressing Lower 1 3 4 4 6 13 . Toileting 1 2 3 3 6 14 . Social Interaction 5 5 6 6 6 15 . Comprehension : A or V 5 5 5 6 6 16 . Expression : V or N 5 5 6 6 6 17 . Problem Solving 4 4 4 5 5 18 . Memory 4 4 4 5 5 Date 12/16 12/19 12/26 01/02 KEY : 7 . Complete independence (timely , safely) 6 . Modified independence (devices needed) 5 . Supervision , standby assist , verbal cues 4 . Minimal assist (subject 75%) 3 . Moderate assist (subject 50%) 2 . Maximal assist (subject 25%) 1 . Total assist (subject 0%) 0 . Activity does not occur Current Patient Status and Results of Treatment : 1 a . Medical : Dr . Paul Miller came to examine patient due to complaints of left hip pain . Bone Scan to be done today . Checking to make sure there isn't any loosening in the surgery area . Has made good progress in overall program . Repeat head CT (to be ordered by Dr . Gandhavadi) Start Prozac today . Patient noting he had been taking it in the past and feels it would benefit his overall mood . Has found himself crying while thinking of current situation . Nursing to report orthostatics blood pressures to Dr . Gandhavadi prior to giving am lisinopril . Use supine blood pressure with medication parameters 1 b . Strength : Improved strength : overall . Spasticity : None present . 1 c . Pain : There are problems with pain in the left hip . Overall the pain is well controlled . Skin : The patient does not have any skin breakdown at this time Sleep : ok with trazadone Comments : Benadryl cream for light rash on right forearm C/ [PATIENT]  headache last night . States it was from getting poor sleep due to having to get up frequently during the night to go to the bathroom . Plan for upcoming week : Continue Nursing Plan of Care (poc) 2 . Bladder and Bowel : Continent of bowel and bladder . 3 a . Nutrition : Appetite : poor Current Diet : carb 3 Calorie Count : not needed Comments : Patient complaining that he just has no desire to eat . Staff encouraging good , nutritional intake Plan for upcoming week : Continue poc 4 . Psychological / Social / Vocational / Spiritual : Participation of/in : Patient   full participation in all therapies . Patient   full participation in nursing education . Social :  [PATIENT]   [PATIENT]  is married and lives alone . He lives in a house . The living area is on more than one level . The bedroom and bathroom are upstairs . There are several steps to enter the home . Patient/family's goals : Return to previous home/apartment . The patient will not have 24 hour supervision/physical assistance available upon discharge . Pts wife is in a long term care facility (MFHCC) and dtrs can only provide intermittent assist . Kathryn J Zerbe , MSW 5 . Mobility : Bed mobility : Patient rolls B directions with stand by assistance Maintained Patient moves supine to/from sit with stand by assistance Maintained Transfers : Patient performs transfers with contact guard assist and verbal cues for safety without assistive device IMPROVED Gait : Patient ambulates 150 feet with contact guard assist with a wheeled walker IMPROVED Stairs : 12 steps , step to pattern , bilateral rails for support requiring minimal assist and verbal cues from therapist . IMPROVED Wheelchair : Patient propels manual wheelchair 125 feet with stand by assist to occasional contact guard assistance for steering IMPROVED Sitting Balance : Static sitting : with stand by assistance without assistive device IMPROVED Dynamic Sitting : with contact guard assist without assistive device IMPROVED Standing Balance : Static Standing : with contact guard assist with hemi walker IMPROVED Dynamic Standing : with minimal assist with hemi walker , sooner to self correct IMPROVED Comments : Jud continues to work hard in therapies and make improvements . He is able to tolerate standing balance activity better . He continues to progress with ambulation using a 2 wheeled walker requiring safe use of it especially with turns and turns to sit . Plan for upcoming week : We will continue to work on improving strength , balance , self adjusting for loss of balance balance , improving activity tolerance , gait mechanics and safety with all mobility .  [12_22_2013]  4 : 47 PM Sandra R Roy , PT Assist 6 . Activities of Daily Living : Eating : modified independent . IMPROVEMENT Grooming : modified independent . IMPROVEMENT Standing at sink with occasional rests needed for back fatigue UB Bathing : requires supervision . LB bathing : requires supervision . Verbal cues with long sponge IMPROVEMENT Peri cares : requires minimal assistance . IMPROVEMENT Clothing retrieval : requires minimal assistance . IMPROVEMENT Upper body dressing : requires supervision . Lower body dressing : requires minimal assistance . Stand to adjust pants : requires minimal assistance . Toileting : requires moderate assistance . Toilet transfers : requires minimal assistance . Tub transfers : requires moderate assistance Comments : Much improved ability to stand and ambulate to the bathroom and complete clothing retrieval . Some improvement in attention to task although continues to need redirection at times . Difficulty with tub transfer possibly due to new situation . C/o pain in left shoulder . States he laid on it poorly during the week and has had increased pain . Seems to improve with some gentle stretching . Plan for upcoming week : increase functional mobility , safety , awareness of self distraction , standing balance and LUE awareness . 7 . Cognition / Communication : Social Interaction : modified independent Comprehension : modified independent IMPROVED Expression : modified independent Problem solving : requires supervision IMPROVED Memory : requires supervision IMPROVED Comments : Completing more complex tasks . Runs into slightly more difficulty when greater interpretation is needed . Improved concentration/mental focus . Able to attend even with background distraction . Uses a line marker for reading . Improved speed and efficiency of task completion is noted . Jud remains motivated , interested in performance and progress . Plan for upcoming week : Cont tx Sandra M Pike , MS , CCC SLP 8 a . Discharge Goals : Acute inpatient rehab goals nearly met and will discharge on Thursday , 1/9 . 8 b . Mobility : General PT Goals : Bed mobility : Patient to roll B directions independently to improve ability to exit/enter bed Patient to move supine to/from sit independently to improve ability to exit/enter from bed Transfers : Patient perform all transfers with modified independence with least restrictive device to decrease caregiver burden and promote in home independence . Gait : Patient will ambulate household distances with modified independence and least restrictive device to allow pt to move around home with decreased assist from family . Sitting Balance : Patient to static sit EOB independently to promote upright posture and preparing for transfer to stand or to chair safely . Patient dynamic sitting balance independently so pt is able to assist in repositioning self in chair and for preparing for transfers . Standing Balance : Static standing balance with modified independence with least restrictive device allow for pt to complete standing tasks without LOB Dynamic standing balance with modified independence with least restrictive device to allow pt to assist with donning and doffing clothing and complete standing tasks with less assist from caregiver . Stairs : Patient will ascend/descend 12 steps single rail and with supervision to safely enter and exit his/her home . Amy C Waters , PT  [12_7_2013]  8 c . Activities of Daily Living (ADL's) : General OT Goals : ADL : Patient will feed self independently . Patient will complete total body bathing with modified independence . Patient will complete total body dressing with modified independence . Patient will complete grooming independently . Patient will perform toileting with modified independence . Mobility : Patient will perform a toilet transfer with modified independence . Patient will peform a tub transfer with stand by assistance . Patient will tolerate standing at the sink for grooming tasks with modified independence . Patient will perform room mobility for ADL with modified independence . Cognition : Pt will be able to complete ADL and simple IADL tasks safely Specialty OT Goals : Patient will complete simple home management task with modified independence . 8 d . Patient and Family Education and Training : Rehabilitation and discharge goals discussed with the patient and/or family . 8 e . Cognition / Communication : ATTENTION : Select attention to task despite distractions , with minimal cues . AUDITORY COMPREHENSION : Increase auditory comprehension of complex paragraph level to 80% accuracy . Increase auditory comprehension of moderate conversation level to 80% accuracy . READING COMPREHENSION : Increase reading comprehension of functional material to 80% accuracy . VERBAL EXPRESSION/MOTOR SPEECH : Will complete moderate formulation tasks at 80 % accuracy . ORIENTATION/MEMORY : Increase recent memory of daily events and conversations to 75% accuracy . Increase recall of novel information post 10 minutes delay with 75% accuracy using memory strategies . PROBLEM SOLVING/ABSTRACT REASONING : Complete moderate level abstract reasoning tasks with 80% accuracy . NUMERICAL REASONING : Complete moderate level math story problems with 75% accuracy . SEQUENCING/ORGANIZATION : Complete moderate level organizational tasks with 80% accuracy . SOCIAL INTERACTION : Will initiate and maintain conversations with 75% with moderate cues . Patient/Family/Team questions or concerns discussed : Patient reported that he had been taking Prozac (20 mlilligrams) in the past . Wondering if he should be back on it again . States he has had some crying episodes recently . Told he shouldn't give up on getting improvement from stroke but that it will take a while . Will start today on Prozac in the morning . 9 . Discharge/Transition Planning : Estimated length of stay : Patient will complete current Rehab program and be discharged to subacute setting on Thursday , 1/9 . Patient indicating preference to go to Menomonee Falls Healthcare Center where his wife is currently residing . Patient states social worker , Heather , could be contacted . Our Social Worker will work with patient and family to make arrangements . Also states he has made arrangements to have Dr .  [XXXXX]  Snyder follow him as primary care physician while in subacute . 9 a . Factors facilitating transition to  [XXXXX]  : Patient Readiness and Family Support 9 b . Barriers to discharge/transition to  [XXXXX]  discussed including Impairments , Participation Restrictions and Activity Limitations . Patient/Family Education Needs Outstanding : Encouraging wife and any other family members to observe patient in therapy sessions to be aware of needs and abilities as he works toward discharge . Participation Restrictions : None Activity Limitations : Self Care : standing tasks , Mobility : transfers and ambulation , Pain : left hip , Tolerance : , and Cognition : safety and ability to attend to task 9 c . Anticipated Discharge Needs : Disposition : Placement in subacute rehabilitation facility . Case Management and Social Work to review patient/family resources and to coordinate Discharge Planning . Anticipated Environmental Needs : 24 hour supervision : , , Physical assistance : , , Equipment : , and Transportation : . Comments : A copy of this note to be provided for patient and/or family in admission folder in patient's room .";
		
		sc = "SAUP # 30362PCHC-01";
		scx = claimNumberRegex(sc);
		
		sc = "CLAIM # A5U31 for patient Jay Urbain";
//		sc = "A5U3140";
		scx = claimNumberRegex(sc);
		
		sc = "30362PCHC-01";
//		sc = "A5U31";
		scx = claimNumberRegex(sc);
		
		sc = "SAUP # 30362PCHC-01 for patient Jay Urbain";
//		sc = "A5U31";
		scx = claimNumberRegex(sc);
		
		sc = "SAUP # A5U3130362pchc-01xx for patient Jay Urbain";
//		sc = "A5U3140";
		scx = claimNumberRegex(sc);
		
		String p0 = "Jay F Urbain  10631972  10/20/1952  DATE OF VISIT: 01/02/2014    1. Aortic stenosis  2. Cerebral vascular disease      Jay Yang Urbain was seen in the general cardiology clinic at the Medical College of Wisconsin on January 2 for followup evaluation of her aortic valve disease.  She was accompanied to today's clinic visit by her husband, son and a Hmong interpreter.  Mrs. Urbain continues to live at home and receives her health care from her family.  Since I initially saw her in October, she has not required hospitalization.  She remains severely limited in her activities because of her right hemiparesis.  She is wheelchair bound and requires help for all her ADLs.  Mrs. Urbain continues to be fed through a G-tube. According to the interpreter and her family, she has not been having symptoms to suggest angina.  She has not experienced presyncope or syncope.  She does sleep with her head elevated, possibly because of shortness of breath but has not been experiencing PND or lower extremity edema.  She ran out of her dextrose strips and has not monitored her blood sugars for the past month.    PHYSICAL EXAMINATION:  Mrs. Urbain was examined in her wheelchair.  VITAL SIGNS:  Blood pressure 130/60, pulse 90.  Weight, she was not weighed.  O2 saturation 95% on room air.  NECK:  Venous pulsations were not seen above the clavicle.  Her carotid upstrokes were difficult to feel and did appear somewhat slow.  I did not appreciate carotid bruits on today's exam.  LUNGS:  Her lungs were clear.  HEART:  The left ventricular impulse was not palpable.  There was no right ventricular heave or precordial thrill.  The apical rhythm was regular.  The first heart sound was soft.  A second heart sound was also soft, with only 1 audible component.  Neither a third or fourth heart sound could be appreciated.  A grade 2/6 to 3/6 mixed frequency systolic ejection murmur was heard over the entire precordium, but heard best at the mid and upper left sternal border.  A diastolic murmur was not apparent.  Neither a third or fourth heart sound was present.  ABDOMEN:  The abdomen was not examined.  EXTREMITIES:  There was no lower extremity edema.    ELECTROCARDIOGRAM:  Today's electrocardiogram shows a sinus rhythm with a frontal axis of -20 degrees.  The PR, QRS intervals are normal.  The corrected QT interval is at the upper limits of normal.  The P-wave morphology is consistent with left atrial enlargement.  T-wave inversion is present in precordial leads V4 through V6 and in I and aVL.  Based upon Cornell voltage criteria, there was LVH.  When compared to her previous ECG, her QT interval has shortened.    IMPRESSION AND RECOMMENDATIONS:  Mrs. Urbain does not appear to have obvious symptoms related to her aortic stenosis.  She has no obvious findings on physical examination to suggest heart failure.  I do not feel that any imaging studies are necessary at this time.  She will be establishing her primary care with Dr. Dangvu later this month.  She will need a basic metabolic panel, a lipid panel and a liver panel at that time.  Provided that she would like to continue her cardiology care at Froedtert Hospital, I have given her an appointment for 4 months.    Home Meds:   Home Medications    Medication Sig Start Date End Date Taking?   acetaminophen (TYLENOL) 500 MG tablet Administer 1 tablet via g-tube every 4 hours as needed. 9/12/13     albuterol (PROVENTIL) (2.5 mg/3mL) 0.083% nebulizer solution Take 3 mL via hand-held nebulizer 4 times daily as needed. 1/2/14     aspirin 81 MG chew tablet Administer 1 tablet via g-tube daily. 9/12/13     atorvastatin (LIPITOR) 80 MG tablet Administer 1 tablet via g-tube nightly. 9/12/13     clopidogrel (PLAVIX) 75 mg tablet Administer 1 tablet via g-tube daily. 9/12/13     famotidine (PEPCID) 20 MG tablet Administer 1 tablet via g-tube daily. 9/12/13     glucagon 1 mg injection Inject 1 mg subcutaneously as needed. 9/12/13     Glucose Blood (BD LOGIC TEST) strip One Touch Ultra Test Strips for insulin sliding scale every 6 hours. Dispense 1 box (100 strips) with 1 refill. 1/2/14 1/2/15    hydrochlorothiazide (HYDRODIURIL) 12.5 MG tablet Administer 1 tablet via g-tube daily. 9/12/13     insulin glargine (LANTUS) 100 UNIT/ML injection Inject 18 Units subcutaneously nightly. 9/12/13     Insulin Regular Human (INSULIN REGULAR - SLIDING SCALE) 100 U/ML injection Inject  subcutaneously every 6 hours (insulin). Inject  subcutaneously every 6 hours (insulin).    BASE DOSE = 8 Units  Correction scale = +2 units  (please see information sheet for full schedule) 9/12/13     losartan (COZAAR) 100 MG tablet Administer 1 tablet via g-tube daily. 9/12/13     magnesium hydroxide (MILK OF MAGNESIA) 400 MG/5ML suspension Administer 30 mL via g-tube nightly as needed. 9/12/13     methylphenidate (RITALIN) 5 MG tablet Take 1 tablet by mouth 2 times daily. at 8AM and 4 PM 11/4/13     metoprolol tartrate (LOPRESSOR) 25 MG tablet Take 1 tablet by mouth every 12 hours. 9/12/13     modafinil (PROVIGIL) 100 MG tablet Administer 1 tablet via g-tube every 12 hours. 9/12/13     sennosides (SENOKOT) 8.8 MG/5ML solution Administer 10 mL via g-tube daily as needed. 9/12/13       No Known Allergies        CC: Huong V. Dangvu, MD  DD: 01/02/2014 17:16  DT: 01/02/2014 18:37";
		String [] p = new String [] {"Urbain,Jay F",null , null,null};
		ArrayList<String> PHI = new ArrayList<>(Arrays.asList("Z1446554"));
		//String [] psplit = p.split("[\\s|\\.|,]+");
		//List<String> patientNameList = new ArrayList<String>(Arrays.asList(psplit));
		//System.out.println(patientNameList);
		
		p = new String[] {"Jay F Urbain",null , null,null};
		//psplit = p.split("[\\s|\\.|,]+");
		//patientNameList = new ArrayList<String>(Arrays.asList(psplit));
		String record = "Jay F Urbain  10631972  10/20/1952  DATE OF VISIT: 01/02/2014    1. Aortic stenosis  2. Cerebral vascular disease      Jay F Urbain was seen in the general cardiology clinic at the Medical College of Wisconsin on January 2 for followup evaluation of her aortic valve disease.  She was accompanied to today's clinic visit by her husband, son and a Hmong interpreter.  Mrs. Urbain continues to live at home and receives her health care from her family.  Since I initially saw her in October, she has not required hospitalization.  She remains severely limited in her activities because of her right hemiparesis.  She is wheelchair bound and requires help for all her ADLs.  Mrs. Urbain continues to be fed through a G-tube. According to the interpreter and her family, she has not been having symptoms to suggest angina.  She has not experienced presyncope or syncope.  She does sleep with her head elevated, possibly because of shortness of breath but has not been experiencing PND or lower extremity edema.  She ran out of her dextrose strips and has not monitored her blood sugars for the past month.    PHYSICAL EXAMINATION:  Mrs. Urbain was examined in her wheelchair.  VITAL SIGNS:  Blood pressure 130/60, pulse 90.  Weight, she was not weighed.  O2 saturation 95% on room air.  NECK:  Venous pulsations were not seen above the clavicle.  Her carotid upstrokes were difficult to feel and did appear somewhat slow.  I did not appreciate carotid bruits on today's exam.  LUNGS:  Her lungs were clear.  HEART:  The left ventricular impulse was not palpable.  There was no right ventricular heave or precordial thrill.  The apical rhythm was regular.  The first heart sound was soft.  A second heart sound was also soft, with only 1 audible component.  Neither a third or fourth heart sound could be appreciated.  A grade 2/6 to 3/6 mixed frequency systolic ejection murmur was heard over the entire precordium, but heard best at the mid and upper left sternal border.  A diastolic murmur was not apparent.  Neither a third or fourth heart sound was present.  ABDOMEN:  The abdomen was not examined.  EXTREMITIES:  There was no lower extremity edema.    ELECTROCARDIOGRAM:  Today's electrocardiogram shows a sinus rhythm with a frontal axis of -20 degrees.  The PR, QRS intervals are normal.  The corrected QT interval is at the upper limits of normal.  The P-wave morphology is consistent with left atrial enlargement.  T-wave inversion is present in precordial leads V4 through V6 and in I and aVL.  Based upon Cornell voltage criteria, there was LVH.  When compared to her previous ECG, her QT interval has shortened.    IMPRESSION AND RECOMMENDATIONS:  Mrs. Urbain does not appear to have obvious symptoms related to her aortic stenosis.  She has no obvious findings on physical examination to suggest heart failure.  I do not feel that any imaging studies are necessary at this time.  She will be establishing her primary care with Dr. Dangvu later this month.  She will need a basic metabolic panel, a lipid panel and a liver panel at that time.  Provided that she would like to continue her cardiology care at Froedtert Hospital, I have given her an appointment for 4 months.    Home Meds:   Home Medications    Medication Sig Start Date End Date Taking?   acetaminophen (TYLENOL) 500 MG tablet Administer 1 tablet via g-tube every 4 hours as needed. 9/12/13     albuterol (PROVENTIL) (2.5 mg/3mL) 0.083% nebulizer solution Take 3 mL via hand-held nebulizer 4 times daily as needed. 1/2/14     aspirin 81 MG chew tablet Administer 1 tablet via g-tube daily. 9/12/13     atorvastatin (LIPITOR) 80 MG tablet Administer 1 tablet via g-tube nightly. 9/12/13     clopidogrel (PLAVIX) 75 mg tablet Administer 1 tablet via g-tube daily. 9/12/13     famotidine (PEPCID) 20 MG tablet Administer 1 tablet via g-tube daily. 9/12/13     glucagon 1 mg injection Inject 1 mg subcutaneously as needed. 9/12/13     Glucose Blood (BD LOGIC TEST) strip One Touch Ultra Test Strips for insulin sliding scale every 6 hours. Dispense 1 box (100 strips) with 1 refill. 1/2/14 1/2/15    hydrochlorothiazide (HYDRODIURIL) 12.5 MG tablet Administer 1 tablet via g-tube daily. 9/12/13     insulin glargine (LANTUS) 100 UNIT/ML injection Inject 18 Units subcutaneously nightly. 9/12/13     Insulin Regular Human (INSULIN REGULAR - SLIDING SCALE) 100 U/ML injection Inject  subcutaneously every 6 hours (insulin). Inject  subcutaneously every 6 hours (insulin).    BASE DOSE = 8 Units  Correction scale = +2 units  (please see information sheet for full schedule) 9/12/13     losartan (COZAAR) 100 MG tablet Administer 1 tablet via g-tube daily. 9/12/13     magnesium hydroxide (MILK OF MAGNESIA) 400 MG/5ML suspension Administer 30 mL via g-tube nightly as needed. 9/12/13     methylphenidate (RITALIN) 5 MG tablet Take 1 tablet by mouth 2 times daily. at 8AM and 4 PM 11/4/13     metoprolol tartrate (LOPRESSOR) 25 MG tablet Take 1 tablet by mouth every 12 hours. 9/12/13     modafinil (PROVIGIL) 100 MG tablet Administer 1 tablet via g-tube every 12 hours. 9/12/13     sennosides (SENOKOT) 8.8 MG/5ML solution Administer 10 mL via g-tube daily as needed. 9/12/13       No Known Allergies        CC: Huong V. Dangvu, MD  DD: 01/02/2014 17:16  DT: 01/02/2014 18:37";
		System.out.println( record );
		record = patientNameList(record, p);
		System.out.println( record );
		
		p = new String[] {"Jay Urbain",null , null,null};
		//psplit = p.split("[\\s|\\.|,]+");
		//patientNameList = new ArrayList<String>(Arrays.asList(psplit));
		record = "Jay F Urbain  10631972  10/20/1952  DATE OF VISIT: 01/02/2014    1. Aortic stenosis  2. Cerebral vascular disease      Jay F Urbain was seen in the general cardiology clinic at the Medical College of Wisconsin on January 2 for followup evaluation of her aortic valve disease.  She was accompanied to today's clinic visit by her husband, son and a Hmong interpreter.  Mrs. Urbain continues to live at home and receives her health care from her family.  Since I initially saw her in October, she has not required hospitalization.  She remains severely limited in her activities because of her right hemiparesis.  She is wheelchair bound and requires help for all her ADLs.  Mrs. Urbain continues to be fed through a G-tube. According to the interpreter and her family, she has not been having symptoms to suggest angina.  She has not experienced presyncope or syncope.  She does sleep with her head elevated, possibly because of shortness of breath but has not been experiencing PND or lower extremity edema.  She ran out of her dextrose strips and has not monitored her blood sugars for the past month.    PHYSICAL EXAMINATION:  Mrs. Urbain was examined in her wheelchair.  VITAL SIGNS:  Blood pressure 130/60, pulse 90.  Weight, she was not weighed.  O2 saturation 95% on room air.  NECK:  Venous pulsations were not seen above the clavicle.  Her carotid upstrokes were difficult to feel and did appear somewhat slow.  I did not appreciate carotid bruits on today's exam.  LUNGS:  Her lungs were clear.  HEART:  The left ventricular impulse was not palpable.  There was no right ventricular heave or precordial thrill.  The apical rhythm was regular.  The first heart sound was soft.  A second heart sound was also soft, with only 1 audible component.  Neither a third or fourth heart sound could be appreciated.  A grade 2/6 to 3/6 mixed frequency systolic ejection murmur was heard over the entire precordium, but heard best at the mid and upper left sternal border.  A diastolic murmur was not apparent.  Neither a third or fourth heart sound was present.  ABDOMEN:  The abdomen was not examined.  EXTREMITIES:  There was no lower extremity edema.    ELECTROCARDIOGRAM:  Today's electrocardiogram shows a sinus rhythm with a frontal axis of -20 degrees.  The PR, QRS intervals are normal.  The corrected QT interval is at the upper limits of normal.  The P-wave morphology is consistent with left atrial enlargement.  T-wave inversion is present in precordial leads V4 through V6 and in I and aVL.  Based upon Cornell voltage criteria, there was LVH.  When compared to her previous ECG, her QT interval has shortened.    IMPRESSION AND RECOMMENDATIONS:  Mrs. Urbain does not appear to have obvious symptoms related to her aortic stenosis.  She has no obvious findings on physical examination to suggest heart failure.  I do not feel that any imaging studies are necessary at this time.  She will be establishing her primary care with Dr. Dangvu later this month.  She will need a basic metabolic panel, a lipid panel and a liver panel at that time.  Provided that she would like to continue her cardiology care at Froedtert Hospital, I have given her an appointment for 4 months.    Home Meds:   Home Medications    Medication Sig Start Date End Date Taking?   acetaminophen (TYLENOL) 500 MG tablet Administer 1 tablet via g-tube every 4 hours as needed. 9/12/13     albuterol (PROVENTIL) (2.5 mg/3mL) 0.083% nebulizer solution Take 3 mL via hand-held nebulizer 4 times daily as needed. 1/2/14     aspirin 81 MG chew tablet Administer 1 tablet via g-tube daily. 9/12/13     atorvastatin (LIPITOR) 80 MG tablet Administer 1 tablet via g-tube nightly. 9/12/13     clopidogrel (PLAVIX) 75 mg tablet Administer 1 tablet via g-tube daily. 9/12/13     famotidine (PEPCID) 20 MG tablet Administer 1 tablet via g-tube daily. 9/12/13     glucagon 1 mg injection Inject 1 mg subcutaneously as needed. 9/12/13     Glucose Blood (BD LOGIC TEST) strip One Touch Ultra Test Strips for insulin sliding scale every 6 hours. Dispense 1 box (100 strips) with 1 refill. 1/2/14 1/2/15    hydrochlorothiazide (HYDRODIURIL) 12.5 MG tablet Administer 1 tablet via g-tube daily. 9/12/13     insulin glargine (LANTUS) 100 UNIT/ML injection Inject 18 Units subcutaneously nightly. 9/12/13     Insulin Regular Human (INSULIN REGULAR - SLIDING SCALE) 100 U/ML injection Inject  subcutaneously every 6 hours (insulin). Inject  subcutaneously every 6 hours (insulin).    BASE DOSE = 8 Units  Correction scale = +2 units  (please see information sheet for full schedule) 9/12/13     losartan (COZAAR) 100 MG tablet Administer 1 tablet via g-tube daily. 9/12/13     magnesium hydroxide (MILK OF MAGNESIA) 400 MG/5ML suspension Administer 30 mL via g-tube nightly as needed. 9/12/13     methylphenidate (RITALIN) 5 MG tablet Take 1 tablet by mouth 2 times daily. at 8AM and 4 PM 11/4/13     metoprolol tartrate (LOPRESSOR) 25 MG tablet Take 1 tablet by mouth every 12 hours. 9/12/13     modafinil (PROVIGIL) 100 MG tablet Administer 1 tablet via g-tube every 12 hours. 9/12/13     sennosides (SENOKOT) 8.8 MG/5ML solution Administer 10 mL via g-tube daily as needed. 9/12/13       No Known Allergies        CC: Huong V. Dangvu, MD  DD: 01/02/2014 17:16  DT: 01/02/2014 18:37";
		System.out.println( record );
		record = patientNameList(record, p);
		System.out.println( record );
		
		String s = "1/1CamelCase03/23PulmonarY01/9/2014Colonoscopy1/27/14 01-23-34-56-78 01234567 8/2012 2/2009";
		
		String s0 = camelCaseAlphaTransition(s);
		System.out.println(s + " : " + s0);
		String s00 = deidentificationRegex.mixedCaseAlphaNumericTransition(s0);
		System.out.println(s0 + " : " + s00);
		
		s = "repeat 2 yearsPAP/pelvic 2014";
		s0 = camelCaseAlphaTransition(s);
		System.out.println(s + " : " + s0);
		
		s = "JWhipple RN ; DWright, RN ; ABryant ; Andie, RN ; JANICE 2694 ; Stephanie 6824 ; Dr Dsrg ; Karen RN ; Sally/Heather RN ; Brian & Lindsey; Pam/Kenny";
		s0 = camelCaseAlphaTransition(s);
		System.out.println(s + " : " + s0);
		s00 = deidentificationRegex.mixedCaseAlphaNumericTransition(s0);
		System.out.println(s0 + " : " + s00);
						
		s = "119 N. Tennyson. 1414 s. Mequon Rd. 12 n 5th Street";
		s00 = deidentificationRegex.addressRegex(s);
		System.out.println(s + " : " + s00);
		
		s = "Colonoscopy 2006Alpha whatever alhaBeta";
		s0 = deidentificationRegex.mixedCaseAlphaNumericTransition(s);
		System.out.println(s + " : " + s0);
		
		s = "declined childbirth classes. 4/1: FAS3/21:  Breastfeeding. Reviewed";
		s0 = deidentificationRegex.dateRegex(s, -1);
		System.out.println(s + " : " + s0);
		
		s = "Mammo: 6-3-13.  5/4/2011 bi mammo (-). 6-5-12. Negative. Had several magnified views";
		String s2 = dateRegex(s, -365);
		System.out.println(s + " : " + s2);
		s2 = deidentificationRegex.compositeRegex(s, -365, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		
		s2 = dateRegex(s, 365);
		System.out.println(s + " : " + s2);
		s2 = deidentificationRegex.compositeRegex(s, 365, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		
		s = "800  Bierman Court - Suite B, Mt. Prospect, IL 60056";
		s2 = dateRegex(s, -10);
		System.out.println(s + " : " + s2);
		s2 = deidentificationRegex.compositeRegex(s, -10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		
		//s = "2/2009";
		s = "12/27/12 12-27-12 7-4, 2013 09/7 10/2014 10/12 9/7 9/2014";
//		s = "Program Coordinator901  N. 9 th Street = Courthouse Rm.   307 A [XXXXX] ,  WI 53233-1967(P)   ";
		s2 = deidentificationRegex.compositeRegex(s, -10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "July 9th 2013 July 9th, 2013 July 9 2013 July 18, 2013";
		s2 = deidentificationRegex.compositeRegex(s, -10, blackListMap, p, PHI);
		System.out.println(s2);
		s = "July 9th 2013";
		s2 = deidentificationRegex.compositeRegex(s, -10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "July 9th, 2013";
		s2 = deidentificationRegex.compositeRegex(s, -10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "July 9 2013";
		s2 = deidentificationRegex.compositeRegex(s, -10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "July 18, 2013";
		s2 = deidentificationRegex.compositeRegex(s, -10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "July 18, 2013";
		s2 = deidentificationRegex.compositeRegex(s, 10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "November 18, 2013";
		s2 = deidentificationRegex.compositeRegex(s, 10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		/////////
		s = "10/2013";
		s2 = deidentificationRegex.compositeRegex(s, 10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "December 2011";
		s2 = deidentificationRegex.compositeRegex(s, 10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "6/22/13";
		s2 = deidentificationRegex.compositeRegex(s, -10, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "10/22/13";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "jay.urbain@gmail.com";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		s = "urbain@msoe.edu";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		
		s = "53217";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		
		s = "53217-1967";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		
		s = "53217 1967";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		
		s = "Jay Urbain's phone number is 414-745-5102, or 745-5102, or (414) 745-5102";
		s2 = deidentificationRegex.phoneNumberRegex(s);
		System.out.println(s + " : " + s2);
		
		s = "Jay Urbain's email  is jay.urbain@gmail.com or urbain@msoe.edu";
		s2 = deidentificationRegex.emailRegex(s);
		System.out.println(s + " : " + s2);	
		
		s = "Matt/George, RN; Matt,RN; mhoag RN; jurbain; G.Kowalski; G.Kowalski,RN; B.TAYLOR RN; BTAYLOR; BTaylor";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "JWhipple RN ; DWright, RN ; ABryant ; Andie, RN ; JANICE 2694 ; Stephanie 6824 ; Dr Dsrg ; Karen RN ; Sally/Heather RN ; Brian & Lindsey; Pam/Kenny";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		
		s = "Matt/George, RN";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "mhoag RN";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "jurbain";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "G.Kowalski";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "G.Kowalski,RN";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "B.TAYLOR RN";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "BTAYLOR";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "BTaylor";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "Matt,RN";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "mhoag RN";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "jurbain";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "G.Kowalski";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "G.Kowalski,RN";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "B.TAYLOR RN";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "BTAYLOR";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "BTaylor";
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "JANICE 2694 ; Stephanie 6824";
		s0 = deidentificationRegex.dateRegex(s, -1);
		System.out.println(s + " : " + s0);
		
		s = "She was last seen in clinic on 9/11/13 for her fourth set of Botox injections to her bilateral neck muscles.";
		s0 = deidentificationRegex.dateRegex(s, -1);
		System.out.println(s + " : " + s0);		
		
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);	
		
		s = "CULT ROUTINE                       COLLECTED: 11/10/04  1445ACCESSION #:  RC-04-41116                STARTED:   11/10/04  1649 \n" + 
				"SOURCE: WOUND \n" + 
				"ANUS   WOUND \n" + 
				"11/10/04  1900 \n" + 
				"----------GRAM STAIN REPORT---------- \n" + 
				"NO PMN'S \n" + 
				"MANY GRAM POSITIVE RODS \n" + 
				"MANY GRAM NEGATIVE RODS \n" + 
				"MODERATE GRAM POSITIVE COCCI \n" + 
				"RARE BUDDING YEAST \n" + 
				"FINAL REPORT                            11/13/04  1018 \n" + 
				"ESCHERICHIA COLI 4+ \n" + 
				"PROTEUS MIRABILIS 2+ \n" + 
				"ENTEROCOCCUS SPECIES 2+ \n" + 
				"CITROBACTER FREUNDII 1+ \n" + 
				"MIXED WITH NORMAL SKIN FLORA 4+ \n" + 
				"E COLI                         MIC MIC INTERP \n" + 
				"______                         ___ __________ \n" + 
				"AMIKACIN               <=2          S \n" + 
				"CEFEPIME               <=4          S \n" + 
				"CEFTRIAXONE            <=8          S \n" + 
				"CEPHALOTHIN              8          S \n" + 
				"CIPROFLOXACIN          >=4          R \n" + 
				"ERTAPENEM            0.016          S \n" + 
				"GENTAMICIN            >=16          R \n" + 
				"IMIPENEM               <=4          S \n" + 
				"MOXIFLOXACIN           >32          R \n" + 
				"PIPER/TAZOBACT         <=8          S \n" + 
				"SXT                    160          R \n" + 
				"TOBRAMYCIN               1          S \n" + 
				"PROMIR                         MIC MIC INTERP \n" + 
				"______                         ___ __________ \n" + 
				"AMIKACIN               <=2          S \n" + 
				"CEFEPIME               <=4          S \n" + 
				"CEFTRIAXONE            <=8          S \n" + 
				"CEPHALOTHIN            <=2          S \n" + 
				"CIPROFLOXACIN        <=0.5          S \n" + 
				"ERTAPENEM            0.012          S \n" + 
				"GENTAMICIN               1          S \n" + 
				"IMIPENEM               <=4          S \n" + 
				"MOXIFLOXACIN             1          S \n" + 
				"PIPER/TAZOBACT         <=8          S \n" + 
				"SXT                   <=10          S \n" + 
				"TOBRAMYCIN               1          S \n" + 
				"C FREUND                       MIC MIC INTERP \n" + 
				"________                       ___ __________ \n" + 
				"AMIKACIN               <=2          S \n" + 
				"CEFEPIME               <=4          S \n" + 
				"CEFTRIAXONE            <=8          S \n" + 
				"CEPHALOTHIN                         R \n" + 
				"CIPROFLOXACIN        <=0.5          S \n" + 
				"ERTAPENEM            0.008          S \n" + 
				"GENTAMICIN           <=0.5          S \n" + 
				"IMIPENEM               <=4          S \n" + 
				"MOXIFLOXACIN         0.064          S \n" + 
				"PIPER/TAZOBACT         <=8          S \n" + 
				"SXT                   <=10          S \n" + 
				"TOBRAMYCIN           <=0.5          S \n" + 
				"ENTERO                         MIC MIC INTERP \n" + 
				"______                         ___ __________ \n" + 
				"AMPICILLIN          <=0.12          S \n" + 
				"VANCOMYCIN           <=0.5          S";
		
		
		s2 = deidentificationRegex.compositeRegex(s, -7, blackListMap, p, PHI);
		System.out.println(s + " : " + s2);
		
		String s3 = mrnRegex(s);
		System.out.println(s + " : " + s3);
		
//		//String s3 = compositeRegex(s, 10);
//		System.out.println(s3);
//		String s4 = "414-745-5102 414 745 5102 (414)745-5102 (414) 745-5102 (414)-745-5102 745-5102 7455102";
////		String s4 = "414-745-5102";
//		System.out.println(s4);
//		//String s5 = compositeRegex(s4, -10);
//		System.out.println(s5);

	}
}
